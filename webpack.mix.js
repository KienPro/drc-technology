const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/dist/js')
    .sass('resources/assets/sass/app.scss', 'public/dist/css')

    //Users
    .js('resources/assets/js/users/create.js', 'public/dist/js/users')
    .js('resources/assets/js/users/edit.js', 'public/dist/js/users')

    //Roles
    .js('resources/assets/js/roles/create.js', 'public/dist/js/roles')
    .js('resources/assets/js/roles/edit.js', 'public/dist/js/roles')

    //Product Categories
    .js('resources/assets/js/product-categories/create.js', 'public/dist/js/product-categories')
    .js('resources/assets/js/product-categories/edit.js', 'public/dist/js/product-categories')

    //Product Models
    .js('resources/assets/js/product-models/create.js', 'public/dist/js/product-models')
    .js('resources/assets/js/product-models/edit.js', 'public/dist/js/product-models')

    //Product Models
    .js('resources/assets/js/product-tags/create.js', 'public/dist/js/product-tags')
    .js('resources/assets/js/product-tags/edit.js', 'public/dist/js/product-tags')

    //Products
    .js('resources/assets/js/products/create.js', 'public/dist/js/products')
    .js('resources/assets/js/products/edit.js', 'public/dist/js/products')

    //Products
    .js('resources/assets/js/slideshows/create.js', 'public/dist/js/slideshows')
    .js('resources/assets/js/slideshows/edit.js', 'public/dist/js/slideshows')

    //Product Images
    .js('resources/assets/js/product-images/create.js', 'public/dist/js/product-images')
    .js('resources/assets/js/product-images/edit.js', 'public/dist/js/product-images')

    //Profiles
    .js('resources/assets/js/profile.js', 'public/dist/js')

    //Receive Payment
    .js('resources/assets/js/payment.js', 'public/dist/js')

    //Dashboard
    .js('resources/assets/js/dashboard.js', 'public/dist/js')
