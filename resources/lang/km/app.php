<?php

return [
	'global' => [
		'search' => 'ស្វែងរក',
		'save' => 'រក្សាទុក',
		'edit' => 'កែប្រែ',
		'cancel' => 'បោះបង់',
		'delete' => 'លុប',
		'add-new' => 'បង្កើតថ្មី',
		'total' => 'សរុប',
		'records' => 'ទិន្ន័យ',
		'back' => 'ត្រឡប់ក្រោយ',
		'save-changes' => 'រក្សាទុក',
		'actions' => 'សកម្មភាព',
		'status' => 'ស្ថានការណ៍',
		'confirm' => 'យល់ព្រម'
	],
	'main-sidebar' => [
		'search' => 'ស្វែងរក',
		'dashboard' => 'ផ្ទាំងគ្រប់គ្រង',
		'manage' => 'ការរៀបចំ',
		'view-order' => 'មើលការកម្មង់ទិញ',
		'product' => 'ផលិតផល',
		'reports' => 'របាយការណ៍',
		'sale-report' => 'របាយការណ៍លក់',
		'customer-report' => 'របាយការណ៍អតិថិជន',
		'payment-report' => 'របាយការណ៍បង់ប្រាក់',
		'setting' => 'ការកំណត់ទាំងអស់',
		'product-category' => 'កាតាឡុក',
		'product-model' => 'ម៉ូឌែល',
		'product-tags' => 'ស្លាកផលិតផល',
		'slideshow' => 'ផ្ទាំងបង្ហាញរូបភាព',
		'role' => 'តួនាទី',
		'user' => 'អ្នកប្រើប្រាស់ប្រព័ន្ធ',
		'profile' => 'គណនី',
		'sign-out' => 'ចាកចេញ',
    ],
	'dashboard' => [
		'dashboard' => 'ផ្ទាំងគ្រប់គ្រង',
		'total-pending-orders' => 'សរុបការបញ្ជាទិញមិនទាន់សម្រេច',
		'total-products' => 'សរុបមុខទំនិញ',
		'total-customers' => 'សរុបអតិថិជន',
		'total-users' => 'សរុបអ្នកប្រើប្រាស់',
		'more-info' => 'ព័ត៌មានបន្ថែម',
		'daily-revenue' => 'ប្រាក់ចំណូលប្រចាំថ្ងៃ'
	],

	'profile' => [
		'save-changes' => 'រក្សាទុក',
		'change-password' => 'ប្តូរពាក្យសម្ងាត់',
		'current-password' => 'ពាក្យសម្ងាត់បច្ចុប្បន្ន',
		'new-password' => 'ពាក្យសម្ងាត់ថ្មី',
		'confirm-password' => 'បញ្ជាក់ពាក្យសម្ងាត់',
		'cancel' => 'បោះបង់',
		'profile-information' => 'ពត័មានប្រូហ្វាល់',
		'first-name' => 'គោត្តនាម',
		'last-name' => 'នាម',
		'browse' => 'អាប់ឡូត',
		'username' => 'ឈ្មោះ​អ្នកប្រើប្រាស់',
		'gender' => 'ភេទ',
		'address' => 'អាសយដ្ឋាន',
		'email' => 'អ៊ីមែល',
		'phone-number' => 'លេខទូរស័ព្ទ',
		'description' => 'ការពិពណ៌នា',
		'upload-profile-picture' => 'អាប់ឡូតរូបភាពប្រូហ្វាល់',
		'save' => 'រក្សាទុក',
    ],

	'users' => [
		'user' => 'អ្នកប្រើប្រាស់',
		'user-information' => 'ពត៍មានអ្នកប្រើប្រាស់',
		'password' => 'ពាក្យសម្ងាត់',
		'username' => 'ឈ្នោះអ្នកប្រើប្រាស់',
		'email' => 'អ៊ីម៉ែល',
		'phone-number' => 'លេខទូរស័ព្ទ',
		'role' => 'តួនាទី',
		'gender' => 'ភេទ',
		'description' => 'ការពិពណ៌នា',
		'delete-user' => 'លុបអ្នកប្រើប្រាស់',
		'message-verify-delete' => 'តើអ្នកពិតជាចង់លុបអ្នកប្រើប្រាស់នេះម៉ែនទេ?'
	],

	'product-models' => [
		'product-model-information' => 'ពត៍មានម៉ូឌែល',
		'product-model' => 'ម៉ូឌែល',
		'name' => 'ឈ្មោះម៉ូឌែល',
		'product-category' => 'កាតាឡុក',
	],

	'product-categories' => [
		'product-category-information' => 'ពត៍មានកាតាឡុក',
		'product-category' => 'កាតាឡុក',
		'name' => 'ឈ្មោះកាតាឡុក',
		'sequence' => 'លំដាប់',
	],

	'product-tags' => [
		'product-tags-information' => 'ពត៍មានស្លាក',
		'product-tags' => 'ស្លាក',
		'name' => 'ឈ្មោះស្លាក',
		'sequence' => 'លំដាប់',
	],

	'products' => [
		'cost' => 'ថ្លៃដើម',
		'product' => 'ផលិតផល',
		'product-information' => 'ពត៍មានផលិតផល',
		'product-category' => 'កាតាឡុក',
		'product-model' => 'ម៉ូឌែល',
		'name' => 'ឈ្មោះផលិតផល',
		'type' => 'ប្រភេទផលិតផល',
		'best-seller' => 'លក់ដាច់',
		'image' => 'រូបភាព',
		'product-tags' => 'ស្លាក',
		'short_description' => 'ការពិពណ៌នាសង្ខេប',
		'long_description' => 'ការពិពណ៌នាលម្អិត',
		'specification' => 'Specification',
		'current-price' => 'តម្លៃបច្ចុប្បន្ន',
		'old-price' => 'តម្លៃដើម',
		're-order-stock' => 'ចំនុចត្រូវទិញស្តុកចូលបន្ថែម',
		'sequence' => 'លំដាប់',
		'description' => 'ការពិពណ៌នា',
		'new' => 'ផលិតផលថ្មី'
	],

	'product-images	' => [
		'product-images' => 'រូបភាពផលិតផល',
		'name' => 'រូបភាព',
		'sequence' => 'លំដាប់',
	],
	'slideshows' => [
		'image' => 'រូបភាព',
		'title' => 'ចំណងជើង',
		'product-name' => 'ផលិតផល',
		'tagline' => 'Tagline',
		'url' => 'url',
		'button' => 'ឈ្មោះប៊ូតុង',
		'sequence' => 'លំដាប់',
		'title-delete' => 'លុប Slideshow',
		'message-delete' => 'តើអ្នកពិតជាចង់លុប slideshow នេះម៉ែនទេ?',
	],
	'view-orders' => [
		'view-orders' => 'View Orders',
		'from-date' => 'ចាប់ពីថ្ងៃ',
		'to-date' => 'ដល់ថ្ងៃ',
		'code' => 'កូដ',
		'customer-name' => 'ឈ្មោះអតិថិជន',
		'email' => 'អ៊ីម៉ែល',
		'phone-number' => 'លេខទូរស័ព្ទ',
		'order-date' => 'កាលបរិច្ឆេទកម្មង់ទិញ',
		'rejected-by' => 'អ្នកបដិសេធ',
		'seller' => 'អ្នកទទួលលក់',
		'order-detail-information' => 'ពត័មានលំអិតការកម្មង់ទិញ',
		'product-name' => 'ឈ្មោះផលិតផល',
		'qty' => 'ចំនួន',
		'price' => 'តម្លៃរាយ',
		'amount' => 'ចំនួនទឹកប្រាក់',
		'payment-method' => 'វិធីបង់ប្រាក់',
		'payment-amount' => 'ចំនួនទឹកប្រាក់',
		'payment-date' => 'ថ្ងៃទទួលប្រាក់',
		'note' => 'ចំណាំ',
		'reference' => 'ឯកសារយោង',
		'create-by' => 'បង្កើតដោយ',
		'create-at' => 'បង្កើត'


	],
	'customer-reports' => [
		'from-date' => 'ចាប់ពីថ្ងៃ',
		'to-date' => 'ដល់ថ្ងៃ',
		'customer-name' => 'ឈ្មោះអតិថិជន',
		'email' => 'អ៊ីម៉ែល',
		'phone-number' => 'លេខទូរស័ព្ទ',
		'registered-date' => 'កាលបរិច្ឆេទចុះឈ្មោះ',
	],
	'payment-reports' => [
		'order-code' => 'លេខកូដបញ្ជាទិញ',
		'customer-name' => 'ឈ្មោះអតិថិជន',
		'email' => 'អ៊ីមែល',
		'phone-number' => 'លេខទូរស័ព្ទ',
		'balance' => 'ប្រាក់ដែលបានបង់',
		'total-amount' => 'ថ្លៃសរុបត្រូវបង់',
	],
	'roles' => [
		'roles' => 'តួនាទី',
		'role' => 'តួនាទី',
		'name' => 'តួនាទី',
		'description' => 'ការពិពណ៌នា',
		'role-information' => 'ពត៍មានតួនាទី',
		'manage-permission' => 'ការផ្តល់សិទ្ធិប្រើប្រាស់ប្រព័ន្ធ',
		'are-you-sure-you-want-to-delete-this-role' => 'តើអ្នកពិតជាចង់លុបតួនាទីនេះម៉ែនទេ?',
		'delete-role' => 'លុបតួនាទី',
	]


];
