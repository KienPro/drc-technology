editRole = new Vue({
    el: '#edit-role',
    data: {
        data: data
    },

    methods: {
        save() {
            axios.post(`${baseURL}/admin/roles/${data.id}`,
                this.data
            ).then(response => {
                if (response.data.success) {                    
                    window.location.href = baseURL+ '/admin/roles';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not update role');
                console.log(error)
            })
        },

        submit() {
            showLoading();
            this.$validator.validate().then((result) => {
                if (!result) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }
            })
        },

        checkModule(module) {
            const checked = $('#module'+module).is(':checked');
            const module_target = $('input[type="checkbox"][data-module='+module+']');
            const module_array = module_target.map(function (idx, ele) {
                return $(ele).val();
            });

            if(checked) {
                this.data.permissions = _.unionWith(this.data.permissions, module_array, function (item1, item2) {
                    return item1 == item2;
                });
            } else {
                this.data.permissions = _.differenceWith(this.data.permissions, module_array, function (item1, item2) {
                    return item1 == item2;
                });
            }

            module_target.prop('checked', checked);
        },

        checkAction(e) {
            const module = $(e.target).data('module');

            $('#module' + module).prop('checked', $('input[type="checkbox"][data-module='+module+']:checked').length === $('input[type="checkbox"][data-module='+module+']').length)
        }
    },

    mounted() {
        const that = this;
        $('input[type="checkbox"][data-main-module]').each(function() {
            let module_array = [];

            $('input[type="checkbox"][data-module='+$(this).data('main-module')+']').each(function () {
                module_array.push(parseInt($(this).val()));
            });

            $(this).prop('checked', _.difference(module_array, that.data.permissions).length === 0)
        })

    }
});
