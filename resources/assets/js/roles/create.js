createRole = new Vue({
    el: '#create-role',
    data: {
        data: {
            permissions: [],
            enable_status : 1
        },
    },

    methods: {
        save() {
            console.log('save');
            axios.post(`${baseURL}/admin/roles`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL+ '/admin/roles';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not add role');
                console.log(error)
            })
        },

        submit() {
            showLoading();
            this.$validator.validate().then((result) => {
                if (!result) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }
            })
        },

        checkModule(module) {
            const checked = $('#module'+module).is(':checked');
            const module_target = $('input[type="checkbox"][data-module='+module+']');
            const module_array = module_target.map(function (idx, ele) {
                                    return $(ele).val();
                                });

            if(checked) {
                this.data.permissions = _.union(this.data.permissions, module_array);
            } else {
                this.data.permissions = _.difference(this.data.permissions, module_array);
            }

            module_target.prop('checked', checked);
        },

        checkAction(e) {
            const module = $(e.target).data('module');

            $('#module' + module).prop('checked', $('input[type="checkbox"][data-module='+module+']:checked').length === $('input[type="checkbox"][data-module='+module+']').length)
        }
    }
});
