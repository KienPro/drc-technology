new Vue({
    el: '#createProductModel',
    data:{
        data:{
            name : "",
            product_category_id : null,
        },
        product_categories : product_categories
    },

    methods: {
        save() {
            axios.post(`${baseURL}/admin/product-models`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL+'/admin/product-models';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not add product model');
                console.log(error)
            })
        },

        submit(){
            showLoading();
            this.$validator.validate().then((result) => {
                if (!result) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }
            })
        },
    }
});