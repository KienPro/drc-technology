new Vue({
    el: '#editProductModel',
    data:{
        data: data,
        product_categories : product_categories
    },

    methods: {
        save() {
            axios.post(`${baseURL}/admin/product-models/${data.id}`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL+'/admin/product-models';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not update product model');
                console.log(error)
            })
        },

        submit(){
            showLoading();
            this.$validator.validate().then((result) =>{
                if (!result) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }    
            })
        }
    }
});