editSlideshow = new Vue({
    el: "#editSlideshow",
    data: {
        image: null,
        error: {
            image:null
        },
        data : data,
    },
    methods: {
        save() {
            axios.post(`${baseURL}/admin/slideshows/${data.id}`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL+'/admin/slideshows';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not update slideshow');
                console.log(error)
            })
        },
        submit() {
            showLoading();
            this.$validator.validate().then((result) => {
                if (!result) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                } 
            })
        }
    }
})
    

