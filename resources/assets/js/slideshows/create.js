createSlideshow = new Vue({
    el: "#createSlideshow",
    data: {
        image: null,
        error:{
            image: null
        },
        data:{
            title: '',
            product_name: '',
            tag_line: '',
            url: '',
            button_name: '',
            sequence : 0,
            enable_status : true,
        }
    },

    methods: {
        save() {
            axios.post(`${baseURL}/admin/slideshows`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL+'/admin/slideshows';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not add slideshow');
                console.log(error)
            })
        },
        submit() {
            showLoading();
            this.$validator.validate().then((result) => {
                let save = true;
                if(!this.data.image) {
                    this.error.image = 'The Image field is required.';
                    save = false;
                }
                if (!result || !save) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }
            })
        }
    }
})
    

