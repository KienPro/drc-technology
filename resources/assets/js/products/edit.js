editProduct = new Vue({
    el: "#editProduct",
    data: {
        image: null,
        secondary_image: null,
        error:{
            image: null,
            secondary_image: null
        },
        data : data,
        product_categories : product_categories,
        product_models : product_models,
        product_tags : product_tags
    },
    methods: {
        save() {
            axios.post(`${baseURL}/admin/products/${data.id}`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL+'/admin/products';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not update product');
                console.log(error)
            })
        },
        submit() {
            showLoading();
            this.$validator.validate().then((result) => {
                if (!result) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                    console.log(this.data);
                } 
            })
        },
        selectProductCategory(){
            this.data.product_model_id = null;
            $('#product_model_id').val(null).trigger('change');
        },
        removeTag(removeTagId) {
            let i = 0;
            const length = this.data.tag_ids.length;
            for(i; i < length ; i++){
                if(this.data.tag_ids[i] == removeTagId){
                    this.data.tag_ids.splice(i, 1);
                    break;
                }
            }
        }

    },

    computed: {
        filtered_product_models(){
            if(this.data.product_category_id == null){
                return [];
            }
            return this.product_models.filter(o => o.product_category_id == this.data.product_category_id);
        }
    }
})
    

