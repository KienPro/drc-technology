createProduct = new Vue({
    el: "#createProduct",
    data: {
        image: null,
        secondary_image: null,
        error:{
            image: null,
            secondary_image: null
        },
        data:{
            name : '',
            product_category_id : null,
            product_model_id : null,
            old_price : 0,
            current_price : 0,
            cost : 0,
            short_description : '',
            long_description : '',
            specification : '',
            sequence : 0,
            is_new : 1,
            is_bestseller : 0,
            tag_ids : [],
            enable_status : true,
        },
        product_categories : product_categories,
        product_models : product_models, 
        product_tags : product_tags

    },

    methods: {
        save() {
            axios.post(`${baseURL}/admin/products`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL+'/admin/products';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not add product');
                console.log(error)
            })
        },
        submit() {
            showLoading();
            this.$validator.validate().then((result) => {
                let save = true;
                if(!this.data.image) {
                    this.error.image = 'The Image field is required.';
                    save = false;
                }
                if(!this.data.secondary_image) {
                    this.error.secondary_image = 'The secondary image field is required.';
                    save = false;
                }
                if (!result || !save) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }
            })
        },
        selectProductCategory(){
            this.data.product_model_id = null;
            $('#product_model_id').val(null).trigger('change');
        },
        removeTag(removeTagId) {
            console.log(removeTagId);
            let i = 0;
            const length = this.data.tag_ids.length;
            for(i; i < length ; i++){
                if(this.data.tag_ids[i] == removeTagId){
                    this.data.tag_ids.splice(i, 1);
                    break;
                }
            }
        }
    },

    computed: {
        filtered_product_models(){
            if(this.data.product_category_id == null){
                return [];
            }
            return this.product_models.filter(o => o.product_category_id == this.data.product_category_id);
        }
    }
})
    

