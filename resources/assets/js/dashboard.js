new Vue({
    el: '#daily_revenue',

    data: {
        daily_revenues_selected_month: 0,
        data:{}
    },

    mounted() {
        this.getDataDashboard();
    },
    methods: {

        getDataDashboard(){
            axios.get(`/admin/dashboard/list`,
            ).then(res => {
                if (res.data.success) {
                    this.data = res.data.data;
                    setTimeout(() => {
                        this.daily_revenues_selected_month = Object.keys(this.data.daily_revenues)[0];
                        this.getDailyRevenue();
                    });
                } else {
                    showAlertError(res.data.message);
                }
            }).catch(err =>{
                console.log(err)
            });
        },

        onMonthlyRevenueChange(){
            this.getDailyRevenue();
        },

        getDailyRevenue() {   
            //-------------
            //- BAR CHART -
            //-------------
            var areaChartData = {
                labels  : this.data.daily_revenues[this.daily_revenues_selected_month].labels,
                datasets: [
                    {
                    label               : 'Daily Revenue',
                    barPercentage       : 0.9,
                    backgroundColor     : 'rgba(60,141,188,0.9)',
                    borderColor         : 'rgba(60,141,188,0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : this.data.daily_revenues[this.daily_revenues_selected_month].data
                    }
                ]
                }
            var barChartCanvas = $('#barChart').get(0).getContext('2d')
            var barChartData = $.extend(true, {}, areaChartData)
            var temp0 = areaChartData.datasets[0]
            areaChartData.datasets[0] = temp0

            var barChartOptions = {
                responsive              : true,
                maintainAspectRatio     : false,
                datasetFill             : false
            }
            if(window.myCharts != undefined)
                window.myCharts.destroy();
                window.myCharts = new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions
            })
        },      
    }

});