new Vue({
    el: '#createProductTags',
    data:{
        data:{
            name : "",
            sequence : 0,
        },
    },

    methods: {
        save() {
            axios.post(`${baseURL}/admin/product-tags`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL+'/admin/product-tags';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not add product tags');
                console.log(error)
            })
        },

        submit(){
            showLoading();
            this.$validator.validate().then((result) => {
                if (!result) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }
            })
        },
    }
});