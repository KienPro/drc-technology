Payment = new Vue({
    el: '#payment',
    data: {
        image: null,
        data:{
            payment_method: '',
            amount: 0,
            payment_date: moment().format('YYYY-MM-DD'),
            note: '',
            image: null
        },
        select_order_code: null
    },
    methods: {
        addPayment(){
            this.data = {
                payment_method: '',
                amount: 0,
                payment_date: moment().format('YYYY-MM-DD'),
                note: ''
            }

            $("#paymentDate")
                .datepicker({
                    orientation: 'bottom',
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                })
                .datepicker("setDate", this.data.payment_date)
                .on("change", () => {this.data.payment_date = $('#paymentDate').val()});

            setTimeout(() => {
                this.$validator.errors.remove('payment_date');
                this.$validator.errors.remove('payment_amount');
                this.$validator.errors.remove('payment_method');
            }, 0);
        },
        save() {

            axios.post(`${baseURL}/admin/view-orders/receive_payment/${this.select_order_code}`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL + '/admin/view-orders';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Cannot add payment');
                console.log(error)
            })
        },
        submit() {
            showLoading();
            this.$validator.validate().then((result) => {
                if (!result) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }
            })
        },
    }
});