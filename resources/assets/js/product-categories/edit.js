new Vue({
    el: '#editProductCategory',
    data:{
        data: data,
    },

    methods: {
        save() {
            axios.post(`${baseURL}/admin/product-categories/${data.id}`,
                this.data
            ).then(response => {
                if (response.data.success) {
                    window.location.href = baseURL+'/admin/product-categories';
                } else {
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not update product category');
                console.log(error)
            })
        },

        submit(){
            showLoading();
            this.$validator.validate().then((result) =>{
                if (!result) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }    
            })
        }
    }
});