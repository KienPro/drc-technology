createProductImage = new Vue({
    el: '#createProductImage',
    data: {
        image:null,
        error:{
            image:null,
        },
        data: {
            product_id: product_id,
            sequence: 0,
            image:null
        },

    },
    methods: {
        save() {
            axios.post(`${baseURL}/admin/product_images`,
                this.data,
            ).then(response => {
                if (response.data.success) {
                    window.location.href =baseURL+'/admin/product_images?product_id=' + this.data.product_id;
                } else {
                    console.log(response.data.message);
                    showAlertError(response.data.message);
                    hideLoading()
                }
            }).catch(error => {
                hideLoading();
                showAlertError('Can not add product');
                console.log(error)
            })
        },

        submit() {
            showLoading();
            this.$validator.validate().then((result) => {
                let save = true;
                if(!this.data.image) {
                    this.error.image = 'The Image field is required.';
                    save = false;
                }
                if (!result || !save) {
                    hideLoading();
                    //set Window location to top
                    window.scrollTo(0, 0);
                } else {
                    this.save();
                }
            })
        },
    }
});
