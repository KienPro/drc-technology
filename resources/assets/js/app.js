window.Vue = require('vue');
window.axios = require('axios');
window._ = require('lodash');

import * as VeeValidate from 'vee-validate';
import CKEditor from 'ckeditor4-vue';

Vue.use( CKEditor );
Vue.use(VeeValidate);

Vue.directive('select2', {
    inserted(el) {
        $(el).on('select2:select', () => {
            const event = new Event('change', {bubbles: true, cancelable: true});
            el.dispatchEvent(event);
        });
    },
});
