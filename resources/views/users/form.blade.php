<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-2">
        <div class="form-group">
          <img id="img-upload" class="profile-user-img img-fluid img-circle mb-1" :src="data.image ? data.image : (data.media ? data.media.url : '{{ asset('adminlte/dist/img/placeholder/square_avatar_placeholder.jpg') }}')" style="width: 120px; height: 120px">
          <input type='file' id="img-input" name="image" class="hide-file-name" accept=".jpg,.png"/>
          <input class="btn-upload btn btn-primary form-control" type="button" value="Browse" onclick="document.getElementById('img-input').value='';document.getElementById('img-input').click();" style="width: 120px;">
        </div>
      </div>
      <div class="col-md-10">
        <div class="form-group">
          <label class="required">{{ __('app.users.username') }}</label>
          <input type="text" name="username" class="form-control" :class="{'is-invalid': errors.has('username') }" v-model="data.username"  v-validate="'required'" placeholder="Enter username"/> 
          <div class="invalid-feedback">@{{ errors.first('username') }}</div>
        </div>
        <div class="form-group">
          <label class="required">{{ __('app.users.phone-number') }}</label>
          <input type="text" name="phone_number" class="form-control" :class="{'is-invalid': errors.has('phone_number') }" v-model="data.phone_number"  v-validate="'required|phone_number'" data-vv-as="phone number" placeholder="Enter phone number"/> 
          <div class="invalid-feedback">@{{ errors.first('phone_number') }}</div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.users.email') }}</label>
      <input type="email" id="email" name="email" class="form-control" :class="{'is-invalid': errors.has('email') }" v-model="data.email"  v-validate="'required'" placeholder="Enter email"/> 
      <div class="invalid-feedback">@{{ errors.first('email') }}</div>
    </div>
  </div>
  @if(isset($data->id))
  <div class="col-md-6">
    <input type="hidden" class="form-control">
  </div>
  @else
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.users.password') }}</label>
      <input type="password" class="form-control" :class="{'is-invalid': errors.has('password') }" name="password" placeholder="********" v-model="data.password" v-validate="'required'">
      <div class="invalid-feedback">@{{ errors.first('password') }}</div>
    </div>
  </div>
  @endif
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.users.gender') }}</label>
      <select name="gender" class="form-control gender-select2" v-select2 v-model="data.gender" style="width: 100%">
        <option value="male">Male</option>
        <option value="female">Female</option>
      </select>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.users.role') }}</label>
      <select class="form-control roles-select2" v-model="data.roles" name="roles" v-select2  multiple="multiple" data-placeholder="Select role" v-validate="'required'" style="width: 100%;">
        <option v-for="role in roles" :value="role.id">@{{ role.name }}</option>
      </select>
      <div class="invalid-feedback">@{{ errors.first('role_id') }}</div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>{{ __('app.users.description') }}</label>
      <textarea name="description" class="form-control" rows="2" placeholder="Type something..." v-model="data.description"></textarea>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label class="inline-checkbox"><input type="checkbox" name="enable_status" v-model="data.enable_status"> Active</label>
    </div>
  </div>
</div>