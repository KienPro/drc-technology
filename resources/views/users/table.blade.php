<div class="table-responsive">
  <table class="table table-hover">
    <thead>
        <tr>
          <th scope="col">#</th>
          <th><div style="width: 170px"></div> {{ __('app.users.username') }}</th>
          <th>{{ __('app.users.email') }}</th>
          <th><div style="width: 160px"></div>{{ __('app.users.phone-number') }}</th>
          <th>{{ __('app.users.role') }}</th>
          <th>{{ __('app.global.status') }}</th>
          @if(checkUserNeedOnePermission($user_per, ['user-update', 'user-delete']))
            <th class="text-center"><div style="width: 160px"></div> {{ __('app.global.actions') }}</th>
          @endif
        </tr>
    </thead>
    <tbody>
    @forelse ($data as $index => $list)
      <tr class="{{ $list->enable_status ? '' : 'text-muted' }}">
        <td>{{ $data->firstItem() + $index }}</td>
        <td>{{ $list->username }}</td>
        <td>{{ $list->email }}</td>
        <td>{{ $list->phone_number }}</td>
        <td>
          @foreach($list->roles as $role)
              {{ $role->name . ($loop->last ? '' : ', ') }}
          @endforeach
        </td>
        <td>{{ $list->enable_status ? "Active" : "Deactive" }}</td>
        @if(checkUserNeedOnePermission($user_per, ['user-update', 'user-delete']))
          <td class="text-center">
            @if(checkUserPermission($user_per, 'user-update'))
              <a href="{{ route('users.edit', $list->id) }}" class="btn btn-info btn-sm" title="Edit"><i class="far fa-edit fa-fw"></i>{{ __('app.global.edit') }}</a>
            @endif
            @if(checkUserPermission($user_per, 'user-delete'))
              <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{ $list->id }}" title="Delete"><i class="far fa-trash-alt fa-fw"></i>{{ __('app.global.delete') }}</button>
              @include('users.delete')
            @endif
          </td>
        @endif
      </tr>
    @empty
    <tr>
      <td colspan="99"><p class="text-center">No record</p></td>
    </tr>
    @endforelse    
    </tbody>
  </table>
</div>