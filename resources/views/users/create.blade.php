@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Users','Users','Create') !!}
  <div class="button pt-3 pl-3">
    <a href="{{ route('users') }}" class="btn btn-default"><i class="fas fa-share fa-flip-horizontal fa-fw"></i>{{ __('app.global.back') }}</a>
    <button type="submit" form="createUser" class="btn btn-primary"><i class="fas fa-save fa-fw"></i>{{ __('app.global.save') }}</button>
  </div>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title text-info"><i class="far fa-info-circle fa-fw"></i>{{ __('app.users.user-information') }}</h3>          
        </div>
        <div class="card-body">
          <form @submit.prevent="submit" id="createUser">
            @include('users.form')
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@include('users.modal-crop-image')
@endsection
@section('footer-content')
  <script>
    let createUser;
    const roles = @json($roles)
  </script>
  <script src="{{ mix('dist/js/app.js') }}"></script>
  <script src="{{ mix('dist/js/users/create.js') }}"></script>
  <script>
    var image_crop = $('#image-crop').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport:{
          width: 300, 
          height: 300, 
          type: 'circle'
        },
        boundary:{
          width: 400, 
          height: 400}
    });    

    $('#img-input').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
            image_crop.croppie('bind', {
                url: event.target.result,
            })
        }
        reader.readAsDataURL(this.files[0]);
        $('#modal-crop-image').modal('show');
    });
   
    $('.submit-crop').click(function(){
        image_crop.croppie('result', {
            type: 'base64',
            size: {width:1080, height:1080},
        }).then(function(res){
            $('#modal-crop-image').modal('hide');
            $('#img-upload').attr('src', res);
            createUser.data.image = res
        })
    })
  
    $('.gender-select2').select2({
        //this code use for hide search box in select2
        minimumResultsForSearch:-1
    });

    $.fn.select2.amd.require(['select2/selection/search'], function (Search) {
        Search.prototype.searchRemoveChoice = function (decorated, item) {
            this.trigger('unselect', {
                data: item
            });

            this.$search.val('');
            this.handleSearch();
        };
    }, null, true);

    $(".roles-select2").select2().on('select2:unselect', function (e) {
        createUser.removeRole(e.params.data.id);
    });
  </script>
@endsection