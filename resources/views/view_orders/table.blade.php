<div class="table-responsive">
  <table class="table table-hover">
    <thead>
      <tr>
      <th>{{ __('app.view-orders.code') }}</th>
      <th><div style="width: 130px"></div> {{ __('app.view-orders.customer-name') }}</th>
      <th>{{__('app.view-orders.email')}}</th>
      <th>{{ __('app.view-orders.phone-number') }}</th>
      <th><div style="width: 130px"></div> {{__('app.view-orders.order-date')}}</th>
      <th>{{ __('app.global.status') }}</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($data as $index => $list)
      <tr id='tr{{$index}}'>
        <td style="color: #337ab7; font-weight: bold">{{ "#".sprintf("%'.06d", $list->code) }}</td>
        <td>{{ $list->customer->name }}</td>
        <td>{{ $list->customer->email }}</td>
        <td>{{ $list->customer->phone_number }}</td>
        <td>{{ $list->date }}</td>
        <td>
          @if ($list->status == 'pending')
            <span class="badge badge-info"><em>{{ $list->status }}</em></span>
          @elseif($list->status == 'confirmed')
            <span class="badge badge-success"><em>{{ $list->status }}</em></span>
          @elseif($list->status == 'rejected')
            <span class="badge badge-danger"><em>{{ $list->status }}</em></span>
          @endif
        </td>
      </tr>
      <tr id="trdetail{{$index}}" class="table-detail o-hide tabs-card-container" style="background-color: #eee; padding: 10px;">
        <td colspan="9">
          <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#tabOrderDetail{{$index}}" role="tab" aria-controls="custom-tabs-four-home" aria-selected="false"><b>Order Detail</b></a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#tabPayment{{$index}}" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false"><b>Payment History</b></a></li>
                </ul>
            </div>
            <div class="card-body">
              <div class="tab-content">
                <div class="tab-pane active" id="tabOrderDetail{{$index}}" role="tabpanel">
                  @include('view_orders.tab_order.order-detail')
                </div>
                <div class="tab-pane" id="tabPayment{{$index}}" role="tabpanel">
                  @include('view_orders.tab_order.payment-history')
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>
      @empty
        <tr>
          <td colspan="99"><p class="text-center">No order</p></td>
        </tr>
      @endforelse
    </tbody>
  </table>
</div>
