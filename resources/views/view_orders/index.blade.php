@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('View Orders','View Orders') !!}
@endsection
@section('content')
<style>
  .o-hide {
      display: none !important;
  }
  .o-show {
      display: show !important;
  }
</style>
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">       
        <div class="card">
          <div class="card-body">
            <form>
              <div class="row">
                <div class="col-md-12">
                  <div class="card bg-light">
                    <div class="card-header">
                      <h3 class="card-title"><i class="fas fa-filter"></i>Filter</h3>
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool view-order-card" data-card-widget="collapse"  data-enable-remember="true">
                          <i class="fas fa-minus"></i>
                        </button>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>{{ __('app.view-orders.from-date') }}</label>
                            <input type="text" name="from_date" class="form-control" value="{{ request('from_date') }}">
                          </div>
                        </div> 
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>{{ __('app.view-orders.to-date') }}</label>
                            <input type="text" name="to_date" class="form-control" value="{{ request('to_date') }}">
                          </div>
                        </div> 
                        <div class="col-md-4">
                          <div class="form-group">
                          <label>{{ __('app.global.status') }}</label>
                            <select name="status" class="form-control status-select2" style="width: 100%">
                              <option value="">All</option>
                              <option value="pending" @if(request('status') == "pending") selected @endif>Pending</option>
                              <option value="confirmed" @if(request('status') == "confirmed") selected @endif>Confirmed</option>
                              <option value="rejected" @if(request('status') == "rejected") selected @endif>Rejected</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>{{ __('app.global.search') }}</label>
                            <input type="text" name="search" class="form-control" value="{{ request('search') }}" placeholder="Order code">
                          </div>
                        </div>
                      </div>
                      <div class="button">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-filter fa-fw"></i>Filter</button>
                        <a href="{{ route('view-orders') }}" class="btn btn-default"><i class="fas fa-sync-alt fa-fw"></i>Clear</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <div id="payment">
              @include('view_orders.table')  
              <form class="form-horizontal" @submit.prevent="submit" id="createPayment" v-cloak>
                @include('view_orders.tab_order.receive-payment')
                @include('view_orders.modal-crop-image')
               </form>

            </div>
            
          </div>
          <div class="card-footer clearfix">
            @include('layouts.pagination')
          </div>
        </div>
      </div>
    </div>
  </div>  
@endsection
@section('footer-content')
<script>
  let Payment;
</script>
<script src="{{ mix('dist/js/app.js') }}"></script>
<script src="{{ mix('dist/js/payment.js') }}"></script>
<script>
    $('input[name="from_date"]').daterangepicker({
    autoApply: true,
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  })

  $('input[name="to_date"]').daterangepicker({
    autoApply: true,
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  })

  $("#paymentDate").datepicker({
      orientation: 'bottom',
      format: 'yyyy-mm-dd',
      autoclose: true,
  })

  $('.status-select2').select2({
    minimumResultsForSearch:-1
  })

  var image_crop = $('#image-crop').croppie({
      enableExif: true,
      enableOrientation: true,
      viewport:{
        width: 300, 
        height: 400, 
        type: 'square'
      },
      boundary:{
        width: 350, 
        height: 450}
  });    

  $('#img-input').on('change', function(){
      var reader = new FileReader();
      reader.onload = function (event) {
          image_crop.croppie('bind', {
              url: event.target.result,
          })
      }
      reader.readAsDataURL(this.files[0]);
      $('#modal-crop-image').modal('show');
  });
  
  $('.submit-crop').click(function(){
      image_crop.croppie('result', {
          type: 'base64',
          size: {width:500, height:550},
      }).then(function(res){
          $('#modal-crop-image').modal('hide');
          $('#img-upload').attr('src', res);
          Payment.data.image = res
      })
  })

  $('.gender-select2').select2({
      //this code use for hide search box in select2
      minimumResultsForSearch:-1
  })

</script>

<!-- <script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script> -->
@foreach($data as $index => $list)
  <script type="text/javascript">
  $('#tr{{$index}}').click(function () {
    if ($('#trdetail{{$index}}').hasClass('o-show')) {
      $('#trdetail{{$index}}').removeClass('o-show');
      $('#trdetail{{$index}}').addClass('o-hide');
    } else {
      $('.table-detail').addClass("o-hide");
      $('#trdetail{{$index}}').removeClass('o-hide');
      $('#trdetail{{$index}}').addClass('o-show');
  }
  });
  </script>
@endforeach
@endsection