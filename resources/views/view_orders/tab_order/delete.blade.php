<div class="modal fade" id="modal-delete-{{ $item->id }}" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ __('dialog_box.delete_title', ['name' => 'Product']) }}</h5>
      </div>
      <form action="{{ route('view-orders.product.delete', $item->id) }}" method="POST">
        @csrf
	    	@method('DELETE')
        <div class="modal-body">
          {{ __('dialog_box.are_you_sure', ['purpose' => 'delete this product']) }}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="far fa-times fa-fw"></i>Cancel</button>
          <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt fa-fw"></i>Delete</button>
        </div>
      </form>
	  </div>
	</div>
</div>
