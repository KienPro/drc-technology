<div class="row">
  <div class="col-md-12" style="margin-top:10px;">
    <table id="example2" class="table table-bordered table-hover">
      <thead class="table-primary">
        <th>#</th>
        <th>{{ __('app.view-orders.reference') }}</th>
        <th>{{ __('app.view-orders.payment-method') }}</th>
        <th>{{ __('app.view-orders.payment-amount') }}</th>
        <th>{{ __('app.view-orders.payment-date') }}</th>
        <th>{{ __('app.view-orders.note') }}</th>
        <th>{{ __('app.view-orders.create-by') }}</th>
        <th>{{ __('app.view-orders.create-at') }}</th>
      </thead>
      <tbody>
        @forelse ($list->payments as $index => $item)
          <tr>
            <td>{{ $data->firstItem() + $index }}</td>
            <td>
              @if ($item->media)
                <a href="{{ asset($item->media->url)}}" data-lity data-lity-target="{{ asset($item->media->url) }}">
                    <img src="{{ asset($item->media->url) }}" class="img-thumbnail" width="60px">
                </a>
              @else
                N/A
              @endif
            </td>
            <td>{{ $item->payment_method }}</td>
            <td class="text-right">{{ formatCurrency($item->amount) }}</td>
            <td>{{ \Carbon\Carbon::parse($item->payment_date)->format('Y-m-d') }}</td>
            <td>{{ $item->note ?? 'N/A' }}</td>
            <td>{{ $item->creator->username ?? null }}</td>
            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('Y-m-d h:i A') }} </td>
          </tr>
        @empty
          <tr>
            <td colspan="99" class="text-center">Not yet payment</td>
          </tr>
        @endforelse
      </tbody>
    </table>
  </div>
</div>
