<div class="row" style="background-color: #fff; padding: 20px">
    <div class="col-md-6 info-line">
        <div class="row" style="border-bottom: solid 1px #ccc; text-align: left;">
        <div class="col-md-6">{{ __('app.view-orders.code') }}</div>
        <div class="col-md-6">
            <label> {{ "#".sprintf("%'.06d", $list->code) }}</label>
        </div>
    </div>    
    <div class="row" style="border-bottom: solid 1px #ccc; text-align: left;">
        <div class="col-md-6 col-sm-6">
           {{ __('app.view-orders.customer-name') }}
        </div>
        <div class="col-md-6 col-sm-6">
            <label>{{ $list->customer->name }}</label>
        </div>
    </div>
    <div class="row" style="border-bottom: solid 1px #ccc; text-align: left;">
        <div class="col-md-6 col-sm-6">
            {{ __('app.global.total') }}
        </div>
        <div class="col-md-6 col-sm-6">
            <label>{{ formatCurrency($list->total) }}</label>
        </div>
    </div>
</div>
<div class="col-md-1"></div>
    <div class="col-md-5 info-line">
        <div class="row" style="border-bottom: solid 1px #ccc; text-align: left;">
            <div class="col-md-6">
                {{ __('app.view-orders.order-date') }}
            </div>
            <div class="col-md-6 col-sm-6">
                <label>{{ $list->date }}</label>
            </div>
        </div>
        <div class="row" style="border-bottom: solid 1px #ccc; text-align: left;">
            <div class="col-md-6 col-sm-6">
                {{ __('app.global.status') }}
            </div>
            <div class="col-md-6 col-sm-6">
                <label><em>{{ $list->status }}</em></label>
            </div>
        </div>
        <div class="row" style="border-bottom: solid 1px #ccc; text-align: left;">
            <div class="col-md-6 col-sm-6">
                @if($list->status == 'rejected')
                    {{ __('app.view-orders.rejected-by') }}
                @else
                    {{ __('app.view-orders.seller') }}
                @endif
            </div>
            <div class="col-md-6 col-sm-6">
                <label>{{ $list->user->username ?? 'N/A' }}</label>
            </div>
        </div>
    </div>  
   
    <div class="col-md-12" style="margin-top:10px;">
        <table class="table table-bordered table-hover">
            <thead class="table-primary">
                <th class="text-center">#</th>
                <th>{{ __('app.view-orders.product-name') }}</th>
                <th class="text-center">{{ __('app.view-orders.qty') }}</th>
                <th class="text-right">{{ __('app.view-orders.price') }}</th>
                <th class="text-right">{{ __('app.view-orders.amount') }}</th>
                <th class="text-center">Actions</th>
            </thead>
            <tbody>
                @foreach($list->order_details as $index => $item)
                <tr>
                    <td class="text-center">{{ $data->firstItem() + $index }}</td>
                    <td>{{ $item->product_name }}</td>
                    <td class="text-center">{{$item->qty}}</td>
                    <td class="text-right">{{formatCurrency($item->price)}}</td>
                    <td class="text-right">{{formatCurrency($item->amount)}}</td>
                    <td class="text-center">
                          <a href="#modal-delete-{{ $item->id }}" class="btn btn-danger btn-sm" data-toggle="modal" title="Remove">
                            <i class="far fa-trash-alt"></i>
                         </a>
                         @include('view_orders.tab_order.delete')
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @if($list->status == 'pending')
    <div class="row">
        <div class="col-md-12">
            @if(checkUserPermission($user_per, 'order-update'))
                <a href="#modelConfirm_{{$list->id}}" class="btn btn-primary btn-sm " data-toggle="modal" style="margin-top: 20px;">
                    Confirm
                </a>
                <a href="#modelReject_{{$list->id}}" class="btn btn-danger btn-sm  delete-button" data-toggle="modal" style="margin-right: 5px; margin-top: 20px" >
                    Reject
                </a>
                <div class="modal fade" id="modelReject_{{$list->id}}" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Reject Order</h4>
                            </div>
                            <div class="modal-body">
                                Are you sure, you want to reject this order {{ "#".sprintf("%'.06d", $list->code) }}?
                            </div>
                            <div class="modal-footer">
                                <form action="{{ url('admin/view-orders/' . $list->id) }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="user_id" value="{{ $auth->user->id }}">
                                    <input type="hidden" name="status" value="rejected">
                                    <button type="submit" class="btn btn-danger">Confirm</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modelConfirm_{{ $list->id }}" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Confirmation Order</h4>
                            </div>
                            <div class="modal-body">
                                You are confirming order {{ "#".sprintf("%'.06d", $list->code) }}
                            </div>
                            <div class="modal-footer">
                                <form action="{{ url('admin/view-orders/' . $list->id) }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="user_id" value="{{ $auth->user->id }}">
                                    <input type="hidden" name="status" value="confirmed">
                                    <button type="submit" class="btn btn-primary">Confirm</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @elseif($list->status == 'confirmed')
    <div  class="row" id="payment">
        <div class="col-md-12">
            <a href="{{ route('view-orders.invoice', $list->code) }}" target="_blank" class="btn btn-warning"><i class="fas fa-print fa-fw" title="Print"></i>Print</a>
            @if(checkUserPermission($user_per, 'order-receive'))
                <a data-toggle="modal" data-target="#receive-payment" @click="select_order_code = {{$list->code}}; addPayment" class="btn btn-success"><i class="fas fa-money-bill fa-fw" title="Receive Payment"></i>Receive Payment</a>
            @endif
        </div>
    </div>
    @endif
</div>

