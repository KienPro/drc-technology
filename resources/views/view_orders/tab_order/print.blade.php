<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>DRC TECHNOLOGY</title>

  <!-- Favicon Icon -->
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('adminlte/dist/img/drc-logo.JPG') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/invoice.css') }}">
</head>
<body onload="window.print()">
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-2">
                    <img src="{{ asset('adminlte/dist/img/drc-transparent-logo.png') }}" class="float-left logo" width="150px">
                </div>
                <div class="col-md-6 col-sm-6">
                    <h3><b>DRC Computer Technology</b></h3>
                    <div class="contact">
                        <p>#686A+681A, St. 21, Phum Thmey, Krong Takhmau,</p>
                        <p>Kandal Province, Cambodia</p>
                        <p>Phone: 012/081 47-57-68</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <h1 class="float-right invoice"><b>INVOICE</b></h1>
                </div>
            </div>
    
            <hr>
        </div>
    </header>
    
    <div class="contact-area">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-2"></div>
                
                <div class="col-md-6 col-sm-6">
                    <div class="client">
                        <p><span><b>Sold to</b>:</span> {{ $data->customer->name }}</p>
                        <p><span><b>Address</b>:</span> {{ $data->customer->address }}</p>
                        <p><span><b>Mobile</b>:</span> {{ $data->customer->phone_number }}</p>
                        <p><span><b>Email</b>:</span> {{ $data->customer->email }}</p>
                    </div>
                </div>
        
                <div class="col-md-4 col-sm-4">
                    <div class="admin">
                        <p><span><b>Invoice</b>:</span> {{ '#DRC'.sprintf("%'.05d", $data->code) }}</p>
                        <p><span><b>Date</b>:</span> {{ $data->order_date }}</p>
                        <p><span><b>Sale Rep</b>:</span> {{ $data->seller }}</p>
                        <p><span><b>Mobile Phone</b>:</span> {{ $data->seller_phone }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="product">
        <div class="container">
            <table class="table table-bordered" style="border: 0">
                <thead>
                    <tr>
                        <th scope="col">N<sup>o</sup></th>
                        <th scope="col">Picture</th>
                        <th scope="col">Model and Description</th>
                        <th scope="col">Qty</th>
                        <th scope="col">Price</th>
                        <th scope="col">Amount</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($data->products as $index =>$list)
                  <tr>
                      <td style="text-align: center">{{ $index + 1 }}</td>
                      <td style="text-align: center"><img class="product-img" src="{{ $list->product->media->url }}" alt=""></td>
                      <td>
                          <h5 class="product-name">{{ $list->product_name }}</h5>
                          <p>{!! $list->product->specification !!}</p>
                      </td>
                      <td style="text-align: center">{{ $list->qty }}</td>
                      <td style="text-align: right">{{ formatCurrency($list->price) }}</td>
                      <td style="text-align: right">{{ formatCurrency($list->amount) }}</td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="5" class="text-right" scope="row" style="border: none">Total:</th>
                        <td class="colspan text-right"><strong>{{ formatCurrency($data->total) }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="term-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h5>Terms and Conditions</h5>
                    <ol>
                        <li>Equipment Warranty: One(1) Year against manufacturer's defects.</li>
                        <li>Payment term: 50% deposit upon order, 50% upon completion.</li>
                        <li>Warranty is voided if seal broken or altered by someone other than our technicians.</li>
                        <li>Warranty is voided if the seal broken, misused or burned.</li>
                        <li>Customer must check the goods at the time delivery. Goods sold are not refunable.</li>
                        <li>In case customer cancel the order after confirmed. Customer will be fined 15% of total amount in quotation by supplier.</li>
                        <li>DRC Techonogy reserves the right at any time after receipt of your order to accept or decline your order for any reason.</li>
                        <li>Pay Directly to:</li>
                        <ul>
                            <li>ABA BANK: Account No: <b>000 614 978</b>, <b>PICH SAMPHORSNEARY</b></li>
                            <li>ACLEDA Bank Plc: Account No: <b>24000222503414</b>, <b>Mr. PRUM CHANSAMEDY</b></li>
                        </ul>
                    </ol>
                </div>
            </div>
    
            <div class="approval">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="signature">
                                <h5 class="approval-head">Customer's Approval</h5>
                                <p class="author-name">Authorized Signature</p>
                            </div>
                        </div>
    
                        <div class="col-md-6 col-sm-6">
                            <div class="signature">
                                <h5 class="approval-head">DRC Computer Technology</h5>
                                <p class="author-name">Authorized Signature</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>