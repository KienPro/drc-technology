<div class="modal fade" id="receive-payment" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Cash Recieved</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row pb-5">
          <div class="col-md-8">
            <div class="form-group row">
              <label for="paymentMethod" class="col-md-3 offset-md-1 col-form-label required">{{ __('app.view-orders.payment-method') }}</label>
              <div class="col-md-8">
                <input type="text" class="form-control" :class="{'is-invalid': errors.has('payment_method') }"  id="payment_method" name="payment_method" v-model="data.payment_method" v-validate="'required'" data-vv-as="payment method"/>
                <div class="invalid-feedback">@{{ errors.first('payment_method') }}</div>
              </div>
            </div>
            <div class="form-group row">
              <label for="paymentAmount" class="col-md-3 offset-md-1 col-form-label required">{{ __('app.view-orders.payment-amount') }}</label>
              <div class="col-md-8">
                <input type="number" class="form-control" :class="{'is-invalid': errors.has('amount') }" step="0.01" id="paymentAmount" name="amount" v-model="data.amount" v-validate="'required'">
                <div class="invalid-feedback">@{{ errors.first('amount') }}</div>
              </div>
            </div>
            <div class="form-group row">
              <label for="paymentDate" class="col-md-3 offset-md-1 col-form-label">{{ __('app.view-orders.payment-date') }}</label>
              <div class="col-md-8">
                <div class="input-group">
                  <input type="text" class="form-control" id="paymentDate" name="payment_date" v-model="data.payment_date">
                  <div class="input-group-append" onclick="$(this).prev('input').focus()" style="cursor: pointer">
                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="note" class="col-md-3 offset-md-1 col-form-label">{{ __('app.view-orders.note') }}</label>
              <div class="col-md-8">
                  <textarea name="" id="note" class="form-control" rows="4" v-model="data.note"></textarea>
              </div>
            </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                <img id="img-upload" :src="data.image ? data.image : (data.media ? data.media.url : '{{ asset('adminlte/dist/img/placeholder/portrait_placeholder.jpg') }}')" style="width: 60%; height: 100%">
                <input type='file' id="img-input" name="image" class="hide-file-name" accept=".jpg,.png"/>
                <input class="btn-upload btn btn-primary form-control" type="button" value="Browse" onclick="document.getElementById('img-input').value='';document.getElementById('img-input').click();" style="width:60%">
              </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('app.global.cancel') }}</button>
        <button type="submit" form="createPayment" class="btn btn-primary">{{ __('app.global.confirm') }}</button>
      </div>
    </div> 
  </div>
</div>