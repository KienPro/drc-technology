<div class="modal" id="modal-crop-image" tabindex="1999" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header text-center d-block">
                <h5 class="modal-title">Upload Reference Image</h5>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="image-crop"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('app.profile.cancel') }}</button>
                <button type="button" class="btn btn-primary submit-crop">{{ __('app.profile.save') }}</button>
            </div>
        </div>
    </div>
</div>