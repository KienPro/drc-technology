@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Roles','Roles') !!}
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h2 class="card-title">Roles</h2>
            <div class="card-tools">
              @if(checkUserPermission($user_per, 'role-create'))
              <a href="{{ route('roles.create') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus fa-fw"></i>{{ __('app.global.add-new') }}</a>
              @endif
            </div>
          </div>
          <div class="card-body">
            @include('roles.table')    
          </div>
          <div class="card-footer clearfix">
             @include('layouts.pagination') 
          </div>
        </div>
      </div>
    </div>
  </div>    
@endsection