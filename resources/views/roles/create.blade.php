@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Roles','Roles','Create') !!}
  <div class="button pt-3 pl-3">
    <a href="{{ route('roles') }}" class="btn btn-default"><i class="fas fa-share fa-flip-horizontal fa-fw"></i>Back</a>
    <button type="submit" form="create-role" class="btn btn-primary"><i class="fas fa-save fa-fw"></i>Save</button>
  </div>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title text-info"><i class="far fa-info-circle fa-fw"></i>{{ __('app.roles.role-information') }}</h3>          
        </div>
        <div class="card-body">
            <form @submit.prevent="submit" id="create-role" v-cloak>
              @include('roles.form')          
            </form>
          </div>  
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer-content')
  <script src="{{ mix('dist/js/app.js') }}"></script>
  <script src="{{ mix('dist/js/roles/create.js') }}"></script>
  <script>
    $('.select2').select2({
      placeholder:'Select an category',
    });
  </script>
@endsection