<table class="table table-hover">
  <thead>
      <tr>
        <th scope="col">#</th>
        <th>{{ __('app.roles.name') }}</th>
        <th>{{ __('app.roles.description') }}</th>
        @if(checkUserNeedOnePermission($user_per, ['role-update', 'role-delete']))
        <th class="text-center">{{ __('app.global.actions') }}</th>
        @endif
      </tr>
  </thead>
  <tbody>
    @foreach ($data as $index => $list)
    <tr>
      <td>{{ $data->firstItem() + $index }}</td>
      <td>{{ $list->name }}</td>
      <td>{{ $list->description }}</td>
      @if(checkUserNeedOnePermission($user_per, ['role-update', 'role-delete']))
      <td class="text-center">
        @if(checkUserPermission($user_per, 'role-update'))
          <a href="{{ route('roles.edit', $list->id) }}" class="btn btn-info btn-sm" title="Edit"><i class="far fa-edit fa-fw"></i>{{ __('app.global.edit') }}</a>
        @endif
        @if(checkUserPermission($user_per, 'role-delete'))
          <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete_role_{{ $list->id }}" title="Delete"><i class="far fa-trash-alt fa-fw"></i>{{ __('app.global.delete') }}</button>
          @include('roles.delete')
        @endif
      </td>
      @endif
    </tr>
    @endforeach
  </tbody>
</table>