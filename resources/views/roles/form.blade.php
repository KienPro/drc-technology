<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group" :class="{'has-error' : errors.first('role')}">
          <label for="role" class="required">{{ __('app.roles.role') }}</label>
          <input type="text" class="form-control" name="role" id="role" v-model="data.name" v-validate="'required'">
          <div class="invalid-feedback">@{{ errors.first('role') }}</div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="description">{{ __('app.roles.description') }}</label>
          <textarea class="form-control" name="description" id="description" v-model="data.description" rows="3"></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <div class="checkbox">
            <label><input type="checkbox" name="enable_status" v-model="data.enable_status">Active</label>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>{{ __('app.roles.manage-permission') }}</label>
      <ul id="permission_list" class="list-unstyled">
        @foreach($permissions as $key => $permission)
          <input type="checkbox" class="main-module-permission" data-main-module="{{ $key }}" id="module{{ $key }}" @click="checkModule('{{ $key }}')" value="{{ $key }}">
        <label for="module{{ $key }}"><span class="text-bold" style="text-transform: capitalize">{{ str_replace('_', ' ', $key) }}</span></label>
          <li class="card card-body">
            <ul class="list-unstyled list-permission-actions">
              @foreach($permission as $action)
                <li class="form-check-inline">
                    <input type="checkbox" data-module="{{ $key }}" @click="checkAction" id="permission{{ $action->id }}" v-model="data.permissions" value="{{ $action->id }}">
                    <label for="permission{{ $action->id }}"><span></span></label>
                    <span class="text-info" style="text-transform: capitalize">{{str_replace('_', ' ', $action->action) }}</span>
                </li>
              @endforeach
            </ul>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>

