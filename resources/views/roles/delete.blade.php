<div class="modal fade" id="delete_role_{{ $list->id }}" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ __('app.roles.delete-role') }}</h5>
        </button>
      </div>
      <form action="{{ route('roles.destroy', $list->id) }}" method="POST">
        @csrf
	    	@method('DELETE')
        <div class="modal-body">
          {{ __('app.roles.are-you-sure-you-want-to-delete-this-role') }}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="far fa-times fa-fw"></i>{{ __('app.global.cancel') }}</button>
          <button type="submit" class="btn btn-warning"><i class="far fa-trash-alt fa-fw"></i>{{ __('app.global.delete') }}</button>
        </div>
      </form>
	  </div>
	</div>
</div>
