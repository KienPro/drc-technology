<div class="table-responsive">
  <table class="table table-hover table-md">
    <thead>
        <tr>
          <th scope="col">#</th>
          <th><div style="width: 190px"></div>{{ __('app.product-categories.name') }}</th>
          <th class="text-center"><div style="width: 130px"></div> {{ __('app.product-categories.sequence') }}</th>
          
          @if(checkUserNeedOnePermission($user_per, ['product_category-update', 'product_category-delete']))
            <th class="text-center"><div style="width: 200px"></div>{{ __('app.global.actions') }}</th>
          @endif
        </tr>
    </thead>
    <tbody>
      @forelse ($data as $index => $list)
        <tr>
          <th>{{ $data->firstItem() + $index }}</th>
          <td>{{ $list->name }}</td>
          <td class="text-center">{{ $list->sequence }}</td>
          @if(checkUserNeedOnePermission($user_per, ['product_category-update', 'product_category-delete']))
          <td class="text-center">
            @if(checkUserPermission($user_per, 'product_category-update'))
              <a href="{{ route('product-categories.edit', $list->id) }}" class="btn btn-info btn-sm" title="Edit"><i class="far fa-edit fa-fw"></i>{{ __('app.global.edit') }}</a>
            @endif
            @if(checkUserPermission($user_per, 'product_category-delete'))
              <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{ $list->id }}" title="Delete"><i class="far fa-trash-alt fa-fw"></i>{{ __('app.global.delete') }}</button>
              @include('product_categories.delete')
            @endif
          </td>
          @endif
        </tr>
      @empty
        <tr>
          <td colspan="99" class="text-center">No Product Category</td>
        </tr> 
      @endforelse    
    </tbody>
  </table>
</div>