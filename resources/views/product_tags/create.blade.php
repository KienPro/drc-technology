@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Product Tags','Product Tags','Create') !!}
  <div class="button pt-3 pl-3">
    <a href="{{ route('product-tags') }}" class="btn btn-default"><i class="fas fa-share fa-flip-horizontal fa-fw"></i>{{ __('app.global.back') }}</a>
    <button type="submit" form="createProductTags" class="btn btn-primary"><i class="fas fa-save fa-fw"></i>{{ __('app.global.save') }}</button>
  </div>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title text-info"><i class="far fa-info-circle fa-fw"></i>{{ __('app.product-tags.product-tags-information') }}</h3>          
        </div>
        <div class="card-body">
          <form @submit.prevent="submit" id="createProductTags">
            @include('product_tags.form')          
          </form>
        </div>  
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer-content')
<script src="{{ mix('dist/js/app.js') }}"></script>
<script src="{{ mix('dist/js/product-tags/create.js') }}"></script>
@endsection