
<div class="table-responsive">
  <table class="table table-hover">
    <thead>
        <tr>
          <th scope="col">#</th>
          <th>{{ __('app.product-tags.name') }}</th>
          <th class="text-center">{{ __('app.product-tags.sequence') }}</th>
          @if(checkUserNeedOnePermission($user_per, ['product_tag-update', 'product_tag-delete']))
            <th class="text-center"><div style="width: 150px"></div>{{ __('app.global.actions') }}</th>
          @endif
        </tr>
    </thead>
    <tbody>
       @forelse ($data as $index => $list)
        <tr>
          <th>{{ $data->firstItem() + $index }}</th>
          <td>{{ $list->name }}</td>
          <td class="text-center"><div style="width: 140px"></div> {{ $list->sequence }}</td>
          @if(checkUserNeedOnePermission($user_per, ['product_tag-update', 'product_tag-delete']))
          <td class="text-center">
            @if(checkUserPermission($user_per, 'product_tag-update'))
              <a href="{{ route('product-tags.edit', $list->id) }}" class="btn btn-info btn-sm" title="Edit"><i class="far fa-edit fa-fw"></i>{{ __('app.global.edit') }}</a>
            @endif
            @if(checkUserPermission($user_per, 'product_tag-update'))
              <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{ $list->id }}" title="Delete"><i class="far fa-trash-alt fa-fw"></i>{{ __('app.global.delete') }}</button>
              @include('product_tags.delete')
            @endif
          </td>
          @endif
        </tr>
      @empty
        <tr>
          <td colspan="99" class="text-center">No Product Tags</td>
        </tr> 
      @endforelse    
    </tbody>
  </table>
</div>