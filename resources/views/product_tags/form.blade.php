<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.product-tags.name') }}</label>
      <input type="text" class="form-control" :class="{'is-invalid': errors.has('name') }" name="name" v-model="data.name" v-validate="'required'" placeholder="Enter tags name">
      <div class="invalid-feedback">@{{ errors.first('name') }}</div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>{{ __('app.product-tags.sequence') }}</label>
      <input type="number" class="form-control" name="sequence" v-model="data.sequence" step="1" placeholder="Enter sequence">
    </div>
  </div>
</div>