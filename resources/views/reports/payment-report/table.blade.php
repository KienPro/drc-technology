<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th><div></div>{{ __('app.payment-reports.order-code') }}</th>
        <th><div></div>{{ __('app.payment-reports.customer-name') }}</th>
        <th><div></div>{{ __('app.payment-reports.phone-number') }}</th>
        <th class="text-right"><div></div>{{ __('app.payment-reports.total-amount') }}</th>
        <th class="text-right"><div></div>{{ __('app.payment-reports.balance') }}</th>
        <th class="text-center"><div></div>{{ __('app.global.status') }}</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($data as $index => $list)
      <tr>
        <th>{{ $data->firstItem() + $index }}</th>
         <td style="color: #337ab7; font-weight: bold">{{ "#".sprintf("%'.06d", $list->order->code) }}</td>
        <td>{{ $list->order->customer->name }}</td>
        <td>{{ $list->order->customer->phone_number }}</td>
        <td class="text-right">{{ formatCurrency($list->order->total) }}</td>
        <td class="{{ $list->balance >= $list->order->total ? 'text-success' : 'text-danger' }}" align="right">{{ formatCurrency($list->balance) }}</td>
        <td class="text-center">
          @if ( $list->balance >= $list->order->total)
            <span class="badge badge-success">Closed</span>
          @else
          <span class="badge badge-secondary text-center px-2">Open</span>
          @endif
        </td>
      </tr>
      @empty
        <tr>
          <td colspan="99" class="text-center">No Payment Found</td>
        </tr>
      @endforelse
    </tbody>
  </table>