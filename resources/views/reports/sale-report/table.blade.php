<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th>Product</th>
        <th><div style="width:200px"></div>Total Item Sold</th>
        <th><div style="width:200px"></div>Total Revenue</th>
        <th><div style="width:150px"></div>Cost of Good Sold</th>
        <th>Sold By</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($data as $index => $list)
      <tr>
        <th>{{ $data->firstItem() + $index }}</th>
        <td>  
            <a href="{{ asset('AdminLTE/dist/img/msips63.jpg') }}" data-lity data-lity-target="{{ asset('AdminLTE/dist/img/msips63.jpg') }}">
                <img src="{{ asset('AdminLTE/dist/img/msips63.jpg') }}" class="img-thumbnail" width="60px">
            </a>
        </td>
        <td>{{ $list->user->username }}</td>
      </tr>
      @empty

      @endforelse
    </tbody>
  </table>