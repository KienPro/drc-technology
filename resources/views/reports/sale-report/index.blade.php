@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Sale Reports','Sale Reports') !!}
@endsection
@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <form>
            <div class="row">
              <div class="col-md-12">
                <div class="card bg-light">
                  <div class="card-header">
                    <h3 class="card-title"><i class="far fa-filter"></i>Filter</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>From Date</label>
                          <input type="text" name="from_date" class="form-control" value="{{ request('from_date') }}">
                        </div>
                      </div> 
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>To Date</label>
                          <input type="text" name="to_date" class="form-control" value="{{ request('to_date') }}">
                        </div>
                      </div> 
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Search</label>
                          <input type="text" name="search" class="form-control" value="{{ request('search') }}">
                        </div>
                      </div> 
                    </div>
                    <div class="button">
                      <button type="submit" class="btn btn-primary"><i class="fas fa-filter fa-fw"></i>Filter</button>
                      <a href="#" class="btn btn-success"><i class="fas fa-file-excel fa-fw"></i>Export</a>
                      <a href="{{ route('reports.sale') }}" class="btn btn-default"><i class="fas fa-sync-alt fa-fw"></i>Clear</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          @include('reports.sale-report.table')    
        </div>
        <div class="card-footer clearfix">
          @include('layouts.pagination')
        </div>
      </div>
    </div>
  </div>
@endsection
@section('footer-content')
<script>
  $('input[name="from_date"]').daterangepicker({
    autoApply: true,
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  })

  $('input[name="to_date"]').daterangepicker({
    autoApply: true,
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  })

  $('.plate-type-select2').select2()
</script>
@endsection

