<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th><div style="width:150px"></div>{{__('app.customer-reports.customer-name')}}</th>
        <th><div style="width:150px"></div>{{ __('app.customer-reports.email') }}</th>
        <th><div style="width:100px"></div>{{ __('app.customer-reports.phone-number') }}</th>
        <th><div style="width:150px"></div>{{ __('app.customer-reports.registered-date') }}</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($data as $index => $list)
      <tr>
        <th>{{ $data->firstItem() + $index }}</th>
        <td>{{ $list->name }}</td>
        <td>{{ $list->email }}</td>
        <td>{{ $list->phone_number }}</td>
        <td>{{ $list->created_at }}</td>
      </tr>
      @empty
          
      @endforelse
      
    </tbody>
  </table>