@extends('layouts.master')
@section('content-header')
{!! generateContentHeader('Dashboard', 'Dashboard') !!}
@endsection
@section('content')
<div class="preloader flex-column justify-content-center align-items-center">
  <img class="animation__shake" src="{{ asset('adminlte/dist/img/drc-transparent-logo.png') }}" alt="AdminLTELogo" height="auto" width="100">
</div>

<div class="container-fluid" id="daily_revenue" v-cloak>
  <div class="row">
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3>@{{ data.total_pending_orders }}</h3>

          <p>{{ __('app.dashboard.total-pending-orders') }}</p>
        </div>
        <div class="icon">
          <i class="far fa-shopping-cart"></i>
        </div>
        <a href="{{ route('view-orders') . "?status=pending" }}" class="small-box-footer">{{ __('app.dashboard.more-info') }}<i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>@{{ data.total_products }}</h3>

          <p>{{ __('app.dashboard.total-products') }}</p>
        </div>
        <div class="icon">
          <i class="far fa-box-full"></i>
        </div>
        <a href="{{ route('products') }}" class="small-box-footer">{{ __('app.dashboard.more-info') }}<i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>@{{ data.total_customers }}</h3>

          <p>{{ __('app.dashboard.total-customers') }}</p>
        </div>
        <div class="icon">
         <i class="far fa-users-crown"></i>
        </div>
        <a href="#" class="small-box-footer">{{ __('app.dashboard.more-info') }}<i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner">
          <h3>@{{ data.total_users }}</h3>

          <p>{{ __('app.dashboard.total-users') }}</p>
        </div>
        <div class="icon">
          <i class="far fa-user-cog"></i>
        </div>
        <a href="{{ route('users') }}" class="small-box-footer">{{ __('app.dashboard.more-info') }}<i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
  </div>
  <div class="row" >
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">{{ __('app.dashboard.daily-revenue') }}</h3>
          <div class="card-tools">
            <select class="form-control" v-model="daily_revenues_selected_month" @change="onMonthlyRevenueChange()" id="monthRevenue">
              <option v-for="(item, month) in data.daily_revenues" :value="month">@{{ month }}</option>
           </select>
          </div>
        </div>
        <div class="card-body">
          <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 572px;" width="715" height="312" class="chartjs-render-monitor"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer-content')
<script src="{{ mix('dist/js/app.js') }}"></script>
<script src="{{ mix('dist/js/dashboard.js') }}"></script>
@endsection