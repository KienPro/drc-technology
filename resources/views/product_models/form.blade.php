<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.product-models.product-category') }}</label>
      <select class="form-control product-category-select2" :class="{'is-invalid': errors.has('product_category_id') }"  v-select2 name="product_category_id" v-model="data.product_category_id" v-validate="'required'" style="width: 100%" data-vv-as="product category id">
        <option v-for="product_category in product_categories" :value="product_category.id">@{{ product_category.name }}</option>
      </select>
      <div class="invalid-feedback">@{{ errors.first('product_category_id') }}</div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.product-models.name') }}</label>
      <input type="text" class="form-control" :class="{'is-invalid': errors.has('name') }" name="name" v-model="data.name" v-validate="'required'" placeholder="Enter model name">
      <div class="invalid-feedback">@{{ errors.first('name') }}</div>
    </div>
  </div>  
</div>