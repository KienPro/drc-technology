<div class="table-responsive">
  <table class="table table-hover">
    <thead>
        <tr>
          <th scope="col">#</th>
          <th>{{ __('app.product-models.name') }}</th>
          <th><div style="width: 140px"></div>{{ __('app.product-models.product-category') }}</th>
          @if(checkUserNeedOnePermission($user_per, ['product_model-update', 'product_model-delete']))
          <th class="text-center"><div style="width: 150px"></div> {{ __('app.global.actions') }}</th>
          @endif
        </tr>
    </thead>
    <tbody>
       @forelse ($data as $index => $list)
        <tr>
          <th>{{ $data->firstItem() + $index }}</th>
          <td>{{ $list->name }}</td>
          <td>{{ $list->product_category->name }}</td>
          @if(checkUserNeedOnePermission($user_per, ['product_model-update', 'product_model-delete']))
          <td class="text-center">
            @if(checkUserPermission($user_per, 'product_model-update'))
              <a href="{{ route('product-models.edit', $list->id) }}" class="btn btn-info btn-sm" title="Edit"><i class="far fa-edit fa-fw"></i>{{ __('app.global.edit') }}</a>
            @endif
            @if(checkUserPermission($user_per, 'product_model-delete'))
              <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{ $list->id }}" title="Delete"><i class="far fa-trash-alt fa-fw"></i>{{ __('app.global.delete') }}</button>
              @include('product_models.delete')
            @endif
          </td>
          @endif
        </tr>
      @empty
        <tr>
          <td colspan="99" class="text-center">No Product Model</td>
        </tr> 
      @endforelse 
    </tbody>
  </table>
</div>