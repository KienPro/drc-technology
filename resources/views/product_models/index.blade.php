@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Product Models','Product Models') !!}
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="card-tools">
              @if(checkUserPermission($user_per, 'product_model-create'))
              <a href="{{ route('product-models.create') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus fa-fw"></i>{{ __('app.global.add-new') }}</a>
              @endif
            </div>
          </div>
          <div class="card-body">
            <form class="mb-3">
              <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4 col-sm-12">
                  <div class="input-group">
                    <input type="text" class="form-control" name="search" value="{{ request('search') }}" placeholder="{{ __('app.global.search') }}">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-outline-secondary"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @include('product_models.table')    
          </div>
          <div class="card-footer clearfix">
             @include('layouts.pagination') 
          </div>
        </div>
      </div>
    </div>
  </div>    
@endsection