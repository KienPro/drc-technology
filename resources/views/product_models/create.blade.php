@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Product Models','Product Models','Create') !!}
  <div class="button pt-3 pl-3">
    <a href="{{ route('product-models') }}" class="btn btn-default"><i class="fas fa-share fa-flip-horizontal fa-fw"></i>{{ __('app.global.back') }}</a>
    <button type="submit" form="createProductModel" class="btn btn-primary"><i class="fas fa-save fa-fw"></i>{{ __('app.global.save') }}</button>
  </div>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title text-info"><i class="far fa-info-circle fa-fw"></i>{{ __('app.product-models.product-model-information') }}</h3>          
        </div>
        <div class="card-body">
          <form @submit.prevent="submit" id="createProductModel" v-cloak>
            @include('product_models.form')          
          </form>
        </div>  
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer-content')
  <script>
    const product_categories = <?php echo json_encode($product_categories); ?>;
  </script>
  <script src="{{ mix('dist/js/app.js') }}"></script>
  <script src="{{ mix('dist/js/product-models/create.js') }}"></script>
  <script>
    $('.product-category-select2').select2({
      placeholder:'Select an product category'
    });
  </script>
@endsection