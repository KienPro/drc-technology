<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Favicon Icon -->
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('adminlte/dist/img/drc-logo.JPG')}}">
  <title>DRC TECHNOLOGY</title>

  <style>
    html,body{
      width:100%;
      height:100%;
    }
    body{display:table}
    .page {
      display:table-cell;
      vertical-align:middle;
      text-align:center;
      width:100%;
      height:100%;
    }
    img{
      width: 300px
    }

    .btn{
      text-decoration-line: none;
      display: inline-block;
      font-weight: 400;
      color: #212529;
      text-align: center;
      vertical-align: middle;
      user-select: none;
      background-color: transparent;
      border: 1px solid transparent;
      padding: .375rem .75rem;
      font-size: 1rem;
      line-height: 1.5;
      border-radius: .25rem;
      transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .btn-primary{
      color: #fff;
      background-color: #007bff;
      border-color: #007bff;
      box-shadow: none;
    }
  </style>
</head>
<body>
  <div class="page">
    <img src="{{ asset('adminlte/dist/img/404-notfound.png') }}"><br>
    <a href="{{ route('dashboard') }}" class="btn btn-primary">Back to dashboard</a>
  </div>
</body>
</html>
