<div class="modal fade" id="modalBestseller_{{ $list->id }}" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Update Product</h5>
      </div>
      <div class="modal-body">
          {{ $list->is_bestseller == 0 ? "Are you sure, you want to update this product to bestseller?" : "Are you sure, you want update this product to normal?"}}
      </div>
        <div class="modal-footer">
            <form action="{{ route('products.update.bestseller', $list->id) }}" method="post">
                @csrf
                @if($list->is_bestseller == 0)
                    <input type="hidden" name="is_bestseller" value="1">
                @else
                    <input type="hidden" name="is_bestseller" value="0">
                @endif
                <button type="submit" class="btn btn-warning">Confirm</button>
                <button type="button" class="btn btn-default " data-dismiss="modal">Cancel</button>
            </form>
        </div>
    </div>
  </div>
</div>