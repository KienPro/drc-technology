<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.products.name') }}</label>
      <input type="text" class="form-control" :class="{'is-invalid': errors.has('name') }" name="name" v-model="data.name" v-validate="'required'" placeholder="Enter product name">
      <div class="invalid-feedback">@{{ errors.first('name') }}</div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.products.cost') }}</label>
      <input type="number" class="form-control" :class="{'is-invalid': errors.has('cost') }" name="cost" v-model="data.cost" v-validate="'required'">
      <div class="invalid-feedback">@{{ errors.first('cost') }}</div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>{{ __('app.products.type') }}</label>
      <select class="form-control is-new-select2" v-select2 v-model="data.is_new" name="is_new" style="width: 100%">
        <option value="1">New</option>
        <option value="2">Used</option>
      </select>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.products.old-price') }}</label>
      <input type="number" class="form-control" :class="{'is-invalid': errors.has('old_price') }" name="old_price" v-model="data.old_price" v-validate="'required'" data-vv-as="old price" step="0.01"> 
      <div class="invalid-feedback">@{{ errors.first('old_price') }}</div>
    </div>
  </div>
  
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.products.product-category') }}</label>
      <select class="form-control product-category-select2" :class="'is-invalid'" @change="selectProductCategory()" v-select2 name="product_category_id" v-model="data.product_category_id" v-validate="'required'" style="width: 100%" data-vv-as="product category id">
        <option v-for="product_category in product_categories" :key="product_category.id" :value="product_category.id">@{{ product_category.name }}</option>
      </select>
      <div class="invalid-feedback" role="alert">@{{ errors.first('product_model_id') }}</div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.products.current-price') }}</label>
      <input type="number" class="form-control" :class="{'is-invalid': errors.has('current_price') }" name="current_price" v-model="data.current_price" v-validate="'required'" data-vv-as="current price" step="0.01">
      <div class="invalid-feedback">@{{ errors.first('current_price') }}</div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label class="required">{{ __('app.products.product-model') }}</label>
      <select class="form-control product-model-select2" :class="'is-invalid'"  v-select2 name="product_model_id"  id="product_model_id" v-model="data.product_model_id" v-validate="'required'" style="width: 100%" data-vv-as="product model id">
        <option v-for="product_model in filtered_product_models" :key="product_model.id" :value="product_model.id">@{{ product_model.name }}</option>
      </select>
      <div class="invalid-feedback">@{{ errors.first('product_model_id') }}</div>
    </div>    
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>{{ __('app.products.product-tags') }}</label>
      <select class="form-control product-tag-select2" v-select2 multiple data-placeholder="Choose some product tags" name="tag_ids" v-model="data.tag_ids" style="width: 100%">
        <option v-for="tag in product_tags" :value="tag.id">@{{ tag.name }}</option>
      </select>
      <div class="invalid-feedback">@{{ errors.first('product_tag_id') }}</div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <label>{{ __('app.products.short_description') }}</label>
    <div class="form-group">
      <textarea class="form-control" name="short_description" rows="4" v-model="data.short_description" placeholder="Short descripition..."></textarea>
    </div>
  </div>
  <div class="col-md-6">
    <label>{{ __('app.products.long_description') }}</label>
    <div class="form-group">
      <textarea class="form-control" name="long_description" rows="4" v-model="data.long_description" placeholder="Long descripition..."></textarea>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-2">
    <div class="form-group">
      <img id="img-upload" class="profile-user-img img-fluid mb-1" :src="data.image ? data.image : (data.media ? data.media.url : '{{ asset('adminlte/dist/img/placeholder/square-placeholder.png') }}')" style="width: 140px; height: 140px">
      <input type='file' id="img-input" name="image" class="hide-file-name" accept=".jpg,.png"/>
      <input class="btn-upload btn btn-primary" :class="{'is-invalid': error.image }" type="button" value="Primary Image" onclick="document.getElementById('img-input').value='';document.getElementById('img-input').click();" style="width: 140px;">
      <div class="invalid-feedback">@{{ error.image }}</div>
    </div>        
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <img id="secondary-upload" class="profile-user-img img-fluid mb-1" :src="data.secondary_image ? data.secondary_image : (data.secondary_media ? data.secondary_media.url : '{{ asset('adminlte/dist/img/placeholder/square-placeholder.png') }}')" style="width: 140px; height: 140px">
      <input type='file' id="secondary-input" name="secondary_image" class="hide-file-name" accept=".jpg,.png"/>
      <input class="btn-upload btn btn-primary" :class="{'is-invalid': error.secondary_image }" type="button" value="Secondary Image" onclick="document.getElementById('secondary-input').value='';document.getElementById('secondary-input').click();" style="width: 140;">
      <div class="invalid-feedback">@{{ error.secondary_image }}</div>
    </div>        
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>{{ __('app.products.specification') }}</label>
      <ckeditor class="" :class="{'is-invalid': errors.has('specification') }" name="specification" v-model="data.specification"></ckeditor>
      <div class="invalid-feedback">@{{ errors.first('specification') }}</div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <div class="checkbox">
        <label class="inline-label"><input type="checkbox" v-model="data.enable_status">Active</label>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <div class="checkbox">
        <label class="inline-label"><input type="checkbox" v-model="data.is_bestseller">Bestseller</label>
      </div>
    </div>
  </div>
</div>
