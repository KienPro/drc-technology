@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Products','Products', 'Edit') !!}
  <div class="button pt-3 pl-3">
    <a href="{{ route('products') }}" class="btn btn-default"><i class="fas fa-share fa-flip-horizontal fa-fw"></i>{{ __('app.global.back') }}</a>
    <button type="submit" form="editProduct" class="btn btn-primary"><i class="fas fa-save fa-fw"></i>{{ __('app.global.save-changes') }}</button>
  </div>
@endsection
@section('content')
<style>
  .cke_contents {
    height: 400px !important;
  }
</style>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title text-info"><i class="far fa-info-circle fa-fw"></i>{{ __('app.products.product-information') }}</h3>          
        </div>
        <div class="card-body">
          <form @submit.prevent="submit" id="editProduct">
            @include('products.form')          
            @include('products.modal-crop-image')
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer-content')
<script>
  const data = <?php echo json_encode($data); ?>;
  const product_categories = <?php echo json_encode($product_categories); ?>;
  const product_models = <?php echo json_encode($product_models); ?>;
  const product_tags = <?php echo json_encode($product_tags); ?>;
  let editProduct;
</script>
<script src="{{ mix('dist/js/app.js') }}"></script>
<script src="{{ mix('dist/js/products/edit.js') }}"></script>
<script>
  var image_crop = $('#image-crop').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport:{width: 300, height: 300},
        boundary:{width: 400, height: 400}
    });    

    var imageBind;
    var isImgInput =0 ;

    $('input[type=file]').change(function(e){
        var idClicked= e.target.id;
        image_crop.croppie('destroy');
        if(idClicked == 'img-input'){
            viewport={width: 300, height: 300};
            isImgInput = 1;
            $('#square').show()
            $('#secondary').hide()
        }else{
            viewport={width: 300, height: 300};
            isImgInput = 0;
            $('#square').hide()
            $('#secondary').show()
        }
        
        image_crop = $('#image-crop').croppie({
            enableExif: true,
            enableOrientation: true,
            viewport: viewport,
            boundary: {width:400, height:400}
        });
    });

    $('#img-input, #secondary-input').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
            imageBind = event.target.result;
            image_crop.croppie('bind', {
                url: event.target.result,
            })
        }
        reader.readAsDataURL(this.files[0]);
        $('#modal-crop-image').modal('show');
    });
    
    $('.submit-crop').click(function(){
        image_crop.croppie('result', {
            type: 'base64',
            size: {width:600, height:600},
            quality: 1,
        }).then(function(res){
            $('#modal-crop-image').modal('hide');
            if(isImgInput){
                $('#img-upload').attr('src', res);
                editProduct.data.image = res
            }
            else{
                $('#secondary-upload').attr('src', res);
                editProduct.data.secondary_image = res
            }
        })
    })

  $('.product-category-select2').select2({
    placeholder:'Choose an product category'
  });
  $('.product-model-select2').select2({
    placeholder:'Choose an product model'
  });
  $.fn.select2.amd.require(['select2/selection/search'], function (Search) {
      Search.prototype.searchRemoveChoice = function (decorated, item) {
          this.trigger('unselect', {
              data: item
          });

          this.$search.val('');
          this.handleSearch();
      };
  }, null, true);

  $(".product-tag-select2").select2().on('select2:unselect', function (e) {
    editProduct.removeTag(e.params.data.id);
  });

</script>
@endsection