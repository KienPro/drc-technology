<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th><div style="width:120px">{{ __('app.products.image') }}</th>
            <th><div style="width:200px">{{ __('app.products.name') }}</th>
            <th>{{ __('app.products.product-category') }}</th>
            <th>{{ __('app.products.product-model') }}</th>
            <th class="text-center"><div style="width:120px">{{ __('app.products.old-price') }}</th>
            <th class="text-center"><div style="width:120px">{{ __('app.products.current-price') }}</th>
            <th><div style="width:120px">{{ __('app.global.status') }}</th>
            @if(checkUserNeedOnePermission($user_per, ['product-update', 'product-delete', 'product-image']))                
                <th class="text-center"><div style="width:210px"></div> {{ __('app.global.actions') }}</th>
            @endif
            </tr>
        </thead>
        <tbody>
            @forelse ($data as $index =>$list)
            <tr>
                <th>{{ $data->firstItem() + $index }}</th>
                <td>
                <a href="{{ asset($list->media->url) ?? 'AdminLTE/dist/img/msips63.jpg' }}" data-lity data-lity-target="{{ asset($list->media->url ?? 'AdminLTE/dist/img/msips63.jpg') }}">
                    <img src="{{ asset($list->media->url)  ?? 'AdminLTE/dist/img/msips63.jpg' }}" class="img-thumbnail" width="60px">
                </a>
                </td>
                <td>
                    {{ $list->name }} <br>
                    @if($list->is_new == 1)
                        <span class="badge badge-danger">{{ __('app.products.new') }}</span>
                    @endif
                    @if($list->is_bestseller == 1)
                    <span class="badge badge-warning">{{ __('app.products.best-seller') }}</span>
                    @endif
                </td>
                <td>{{ $list->product_category->name }}</td>
                <td>{{ $list->product_model->name }}</td>
                <td class="text-center">{{ formatCurrency($list->old_price) }}</td>
                <td class="text-center">{{ formatCurrency($list->current_price) }}</td>
                <td>{{ $list->enable_status ? "Active" : "Deactive" }}</td>
                 @if(checkUserNeedOnePermission($user_per, ['product-update', 'product-delete', 'product-image']))                
                    <td class="text-center">
                        {{-- @if(checkUserPermission($user_per, 'product-image'))                     --}}
                        <a href="#modalBestseller_{{ $list->id }}" class="btn btn-warning btn-sm" data-toggle="modal" title="Bestseller">
                            <i class="far fa-box-check fa-fw"></i>
                        </a>
                        {{-- @endif --}}
                        @if(checkUserPermission($user_per, 'product-image'))                    
                        <a href="{{ route('product_images').'?product_id='. $list->id  }}" class="btn btn-primary btn-sm" title="Image Album"><i class="fas fa-images fa-fw"></i></a>
                        @endif
                        @if(checkUserPermission($user_per, 'product-update'))                    
                            <a href="{{ route('products.edit', $list->id) }}" class="btn btn-info btn-sm" title="Edit"><i class="far fa-edit fa-fw"></i></a>
                        @endif
                        @if(checkUserPermission($user_per, 'product-delete'))
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{ $list->id }}" title="Delete"><i class="far fa-trash-alt fa-fw"></i></button>
                            @include('products.delete')
                        @endif
                        @include('products.bestseller')

                    </td>
                @endif
            </tr>
            @empty
            <tr>
                <td colspan="99" class="text-center">No Product</td>
            </tr> 
            @endforelse
        </tbody>
    </table>
</div>