@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Product Image','Product Image','Create') !!}
  <div class="button pt-3 pl-3">
    <a href="{{ route('product_images').'?product_id='. request('product_id') }}" class="btn btn-default"><i class="fas fa-share fa-flip-horizontal fa-fw"></i>{{ __('app.global.back') }}</a>
    <button type="submit" form="createProductImage" class="btn btn-primary"><i class="fas fa-save fa-fw"></i>{{ __('app.global.save') }}</button>
  </div>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title text-info"><i class="far fa-info-circle fa-fw"></i>Product - ID:{{Request('product_id')}}</h3>          
        </div>
        <div class="card-body">
          <form @submit.prevent="submit" id="createProductImage" v-cloak>
            @include('products.images.form')   
            @include('products.images.modal-crop-image')
          </form>
        </div>  
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer-content')
  <script>
    const product_id = <?php echo Request('product_id'); ?>;
  </script>
  <script src="{{ mix('dist/js/app.js') }}"></script>
  <script src="{{ mix('dist/js/product-images/create.js') }}"></script>
  <script>
    var image_crop = $('#image-crop').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport:{
          width: 300, 
          height: 300, 
          type: 'square'
        },
        boundary:{
          width: 400, 
          height: 400}
    });    

    $('#img-input').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
            image_crop.croppie('bind', {
                url: event.target.result,
            })
        }
        reader.readAsDataURL(this.files[0]);
        $('#modal-crop-image').modal('show');
    });
   
    $('.submit-crop').click(function(){
        image_crop.croppie('result', {
            type: 'base64',
            size: {width:600, height:600},
        }).then(function(res){
            $('#modal-crop-image').modal('hide');
            $('#img-upload').attr('src', res);
            createProductImage.data.image = res
        })
    });
  </script>
@endsection
