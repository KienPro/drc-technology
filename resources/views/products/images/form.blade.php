<div class="row">
  <div class="col-md-2">
    <div class="form-group">
      <img id="img-upload" class="profile-user-img img-fluid mb-1" :src="data.image ? data.image : (data.media ? data.media.url : '{{ asset('adminlte/dist/img/placeholder/square-placeholder.png') }}')" style="width: 150px; height: 150px">
      <input type='file' id="img-input" name="image" class="hide-file-name" accept=".jpg,.png"/>
      <input class="btn-upload btn btn-primary form-control" :class="{'is-invalid': error.image }" type="button" value="Browse" onclick="document.getElementById('img-input').value='';document.getElementById('img-input').click();" style="width: 150px;">
      <div class="invalid-feedback">@{{ error.image }}</div>
    </div>        
  </div>
</div>
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      <label>Display Sequence</label>
      <input type="number" class="form-control" name="sequence" v-model="data.sequence" step="1" placeholder="Enter sequence">
    </div>
  </div>
</div>    
  