<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th>Image</th>
      <th class="text-center">Sequence</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($product_images as $index =>$list)
    <tr>
      <th>{{ $index + 1 }}</th>
      <td>
        <a href="{{ asset($list->media->url) }}" data-lity data-lity-target="{{ asset($list->media->url) }}">
          <img src="{{ asset($list->media->url) }}" class="img-thumbnail" width="60px">
        </a>
      </td>
      <td class="text-center">{{ $list->sequence }}</td>
      <td class="text-center">
        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{ $list->id }}" title="Delete"><i class="far fa-trash-alt fa-fw"></i>Delete</button>
        @include('products.images.delete')
      </td>
    </tr>
    @empty
    <tr>
      <td colspan="99" class="text-center">No Product Image</td>
    </tr> 
    @endforelse       
  </tbody>
</table>