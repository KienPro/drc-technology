@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Products Image','Product Image') !!}
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h2 class="card-title">Products Images</h2>
            <div class="card-tools">
              <a href="{{ route('product_images.create'). '?product_id=' . request('product_id') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus fa-fw"></i>Add New</a>
            </div>
          </div>
          <div class="card-body">
            @include('products.images.table')    
          </div>
        </div>
      </div>
    </div>
  </div>    
@endsection
