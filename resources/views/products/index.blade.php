@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Products','Products') !!}
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="card-tools">
              @if(checkUserPermission($user_per, 'product-create'))
                <a href="{{ route('products.create') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus fa-fw"></i>{{ __('app.global.add-new') }}</a>
              @endif
            </div>
          </div>
          <div class="card-body">
            <form>
              <div class="row">
                <div class="col-md-12">
                  <div class="card bg-light">
                    <div class="card-header">
                      <h3 class="card-title"><i class="fas fa-filter"></i>Filter</h3>
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                          <i class="fas fa-minus"></i>
                        </button>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>{{ __('app.products.product-category') }}</label>
                            <select name="product_category_id" class="form-control product-category-select2" style="width: 100%">
                              <option value="">All</option>
                              @foreach ($product_categories as $product_category)
                                <option value="{{ $product_category->id }}" @if(request('product_category_id') == $product_category->id) selected @endif>{{ $product_category->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>{{ __('app.products.product-model') }}</label>
                            <select name="product_model_id" class="form-control product-model-select2"  style="width: 100%">
                              <option value="">All</option>
                              @foreach ($product_models as $product_model)
                                <option value="{{ $product_model->id }}" @if(request('product_model_id') == $product_model->id) selected @endif>{{ $product_model->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>{{ __('app.global.search') }}</label>
                            <input type="text" name="search" value="{{ request('search') }}" class="form-control" placeholder="Search...">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>{{ __('app.products.type') }}</label>
                            <select name="is_new" class="form-control is-new-select2" style="width: 100%">
                              <option value="">All</option>
                              <option value="1" @if(request('is_new') == "1") selected @endif>New</option>
                              <option value="0" @if(request('is_new') == "0") selected @endif>Used</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>{{ __('app.global.status') }}</label>
                            <select name="enable_status" class="form-control enable_status-select2" style="width: 100%">
                              <option value="">All</option>
                              <option value="1" @if(request('enable_status') == "1") selected @endif>Active</option>
                              <option value="0" @if(request('enable_status') == "0") selected @endif>Deactive</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>{{ __('app.products.best-seller') }}</label>
                            <select name="is_bestseller" class="form-control is_bestseller-select2" style="width: 100%">
                              <option value="">All</option>
                              <option value="1" @if(request('is_bestseller') == "1") selected @endif>Bestseller</option>
                              <option value="0" @if(request('is_bestseller') == "0") selected @endif>Normal</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="button">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-filter fa-fw" title="Filter"></i>Filter</button>
                        @if(checkUserPermission($user_per, 'product-export'))
                          <a href="{{ route('products.export.excel') . "?product_category_id=" . request('product_category_id') . "&product_model_id=" . request('product_model_id') . "&search=" . request('search') . "&is_new=" . request('is_new') . "&enable_status=" . request('enable_status') . "&is_new=" . request('is_new') . "&is_bestseller=" . request('is_bestseller') }}" class="btn btn-success"><i class="fas fa-file-excel fa-fw" title="Export excel"></i>Excel</a>
                        @endif
                        <a href="{{ route('products') }}" class="btn btn-default"><i class="fas fa-sync-alt fa-fw" title="Clear"></i>Clear</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @include('products.table')    
          </div>
          <div class="card-footer clearfix">
             @include('layouts.pagination') 
          </div>
        </div>
      </div>
    </div>
  </div>    
@endsection
@section('footer-content')
<script>
  $('.product-category-select2').select2()
  $('.product-model-select2').select2()
  $('.is-new-select2').select2({
    minimumResultsForSearch:-1
  });

  $('.enable_status-select2').select2({
    minimumResultsForSearch:-1
  });

  $('.is_bestseller-select2').select2({
    minimumResultsForSearch:-1
  });


  </script>
@endsection
