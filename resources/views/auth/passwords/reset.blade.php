
@extends('layouts.login_master')
@section('content')
<div class="login-box">  
  <p class="login-box-msg">Reset password</p>
  <form method="POST" action="{{ url('reset') }}">
    @csrf
    @php $error = session()->get('error'); @endphp
    @include('auth.alert_error_message')

    <input type="hidden" name="token" value="{{ $token }}">
    <input type="hidden" name="email" value="{{ $email }}">

    <div class="form-group">
      <label for="password">Password</label>
      <input id="password" type="password" class="form-control {{ isset($error['val']['password']) ? 'is-invalid' : '' }}" name="password" placeholder="Password">
      <span class="invalid-feedback" role="alert">
        <strong>{{ $error['val']['password'] ?? ''  }}</strong>
      </span>
    </div>

    <div class="form-group">
      <label for="password-confirm">Confirm Password</label>
      <input id="password-confirm" type="password" class="form-control" name="confirm_password"  {{ isset($error['val']['confirm_password']) ? 'is-invalid' : '' }} placeholder="Confirm password">
    </div>

    <div class="row">
      <div class="col-12">
        <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
      </div>
    </div>
  </form>
</div>
@endsection
