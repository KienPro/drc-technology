@extends('layouts.login_master')
@section('content')
<div class="login-box">
  
  <form method="POST" action="{{ url('/verify') }}">
    @csrf

    <div class="alert alert-info alert-dismissible">
        <p>We've sent a verfication code to your email - <strong class="text-center">{{ request('email') }}</strong></p>
    </div>
    @include('auth.alert_error_message')

    @php $error = session()->get('error'); @endphp
    <input type="hidden" name="email" value="{{ request('email') }}">
    <div class="form-group">
      <input type="text" class="form-control @error('verify_code') is-invalid @enderror" name="verify_code" placeholder="Enter verify code">
      @error('verify_code')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>
    <div class="row">
      <div class="col-12">
        <button type="submit" class="btn btn-primary btn-block">Submit</button>
      </div>
    </div>
  </form>
</div>
@endsection
