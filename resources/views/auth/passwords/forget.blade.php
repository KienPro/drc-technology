@extends('layouts.login_master')
@section('content')
<div class="login-box">
  
  <p class="login-box-msg">Reset password</p>
  @include('auth.alert_error_message')

  @php $error = session()->get('error'); @endphp
  <form method="POST" action="{{ url('/forget') }}">
    @csrf

    <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email">
      @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>
    <div class="row">
      <div class="col-12">
        <button type="submit" class="btn btn-primary btn-block">Send Reset Password Link</button>
      </div>
    </div>
  </form>
</div>
@endsection
