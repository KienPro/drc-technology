@extends('layouts.login_master')
@section('content')
<div class="login-box">
  <p class="login-box-msg">Sign in to start your session</p>

  <form method="POST" action="{{ url('login') }}">
    @csrf
    @include('auth.alert_error_message')

    @php $error = session()->get('error'); @endphp    

    <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control {{ isset($error['val']['email']) ? 'is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email">
      <span class="invalid-feedback" role="alert">
        <strong>{{ $error['val']['email'] ?? ''  }}</strong>
      </span>
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control {{ isset($error['val']['password']) ? 'is-invalid' : '' }}" name="password" placeholder="Password">
      <span class="invalid-feedback" role="alert">
        <strong>{{ $error['val']['password'] ?? ''  }}</strong>
      </span>
    </div>
    <p class="mb-2">
      <a href="{{ url('/forget') }}">Forgot password?</a>
    </p>
    <div class="row">
      <div class="col-12">
        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
      </div>
    </div>
  </form>
</div>
@endsection

