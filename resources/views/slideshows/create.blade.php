@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Slideshows','Slideshows','Create') !!}
  <div class="button pt-3 pl-3">
    <a href="{{ route('slideshows') }}" class="btn btn-default"><i class="fas fa-share fa-flip-horizontal fa-fw"></i>{{ __('app.global.back') }}</a>
    <button type="submit" form="createSlideshow" class="btn btn-primary"><i class="fas fa-save fa-fw"></i>{{ __('app.global.save') }}</button>
  </div>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title text-info"><i class="far fa-info-circle fa-fw"></i>Slideshow</h3>          
        </div>
        <div class="card-body">
          <form @submit.prevent="submit" id="createSlideshow">
            @include('slideshows.form')          
          </form>
        </div>  
      </div>
    </div>
  </div>
</div>
@include('slideshows.modal-crop-image')
@endsection
@section('footer-content')
  <script>
    let createSlideshow;
  </script>
  <script src="{{ mix('dist/js/app.js') }}"></script>
  <script src="{{ mix('dist/js/slideshows/create.js') }}"></script>
  <script>
    var image_crop = $('#image-crop').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport:{
          width: 780, 
          height: 203, 
          type: 'square'
        },
        boundary:{
          width: 900, 
          height: 234
        }
    });    

    $('#img-input').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
            image_crop.croppie('bind', {
                url: event.target.result,
            })
        }
        reader.readAsDataURL(this.files[0]);
        $('#modal-crop-image').modal('show');
    });
   
    $('.submit-crop').click(function(){
        image_crop.croppie('result', {
            type: 'base64',
            size: {
              width:1920, height:500
            },
        }).then(function(res){
            $('#modal-crop-image').modal('hide');
            $('#img-upload').attr('src', res);
            console.log(res);
            createSlideshow.data.image = res
        })
    });
  </script>
@endsection