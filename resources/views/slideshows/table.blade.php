<div class="table-responsive">
  <table class="table table-hover">
      <thead>
          <tr>
            <th scope="col">#</th>
            <th><div style="width: 120px"></div>{{ __('app.slideshows.image') }}</th>
            <th><div style="width: 120px"></div>{{ __('app.slideshows.title') }}</th>
            <th><div style="width: 120px"></div>{{ __('app.slideshows.product-name') }}</th>
            <th class="text-center">{{ __('app.slideshows.sequence') }}</th>
            <th><div style="width: 120px"></div>{{ __('app.global.status') }}</th>
            @if(checkUserNeedOnePermission($user_per, ['slideshow-update', 'slideshow-delete']))
            <th class="text-center"><div style="width: 150px"></div>{{ __('app.global.actions') }}</th>
            @endif
          </tr>
      </thead>
      <tbody>
        @forelse ($data as $index => $list)
        <tr class="{{ $list->enable_status ? '' : 'text-muted' }}">
            <th>{{ $data->firstItem() + $index }}</th>
            <td>
              <a href="{{ asset($list->media->url) ?? 'adminlte/dist/img/msips63.jpg' }}" data-lity data-lity-target="{{ asset($list->media->url ?? 'AdminLTE/dist/img/msips63.jpg') }}">
                  <img src="{{ asset($list->media->url)  ?? 'adminlte/dist/img/msips63.jpg' }}" class="img-thumbnail" width="60px">
              </a>
            </td>
            <td>{{ $list->title }}</td>
            <td>{{ $list->product_name }}</td>
            <td class="text-center">{{ $list->sequence }}</td>
            <td>{{ $list->enable_status ? 'Active' : 'Deactive' }}</td>
            @if(checkUserNeedOnePermission($user_per, ['slideshow-update', 'slideshow-delete']))
            <td class="text-center">
              @if(checkUserPermission($user_per, 'slideshow-update'))
                <a href="{{ route('slideshows.edit', $list->id) }}" class="btn btn-info btn-sm" title="Edit"><i class="far fa-edit fa-fw"></i>{{ __('app.global.edit') }}</a>
              @endif
              @if(checkUserPermission($user_per, 'slideshow-delete'))
                <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{ $list->id }}" title="Delete"><i class="far fa-trash-alt fa-fw"></i>{{ __('app.global.delete') }}</button>
                @include('slideshows.delete')
              @endif
            </td>
            @endif
          </tr>
        @empty
          <tr>
            <td colspan="99" class="text-center">No Slideshow</td>
          </tr> 
        @endforelse 
      </tbody>
    </table>
</div>