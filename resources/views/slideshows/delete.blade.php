<div class="modal fade" id="modal-delete-{{ $list->id }}" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ __('app.slideshows.title-delete') }}</h5>
        </button>
      </div>
      <form action="{{ route('slideshows.destroy', $list->id) }}" method="POST">
        @csrf
	    	@method('DELETE')
        <div class="modal-body">
          {{ __('app.slideshows.message-delete') }}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="far fa-times fa-fw"></i>{{ __('app.global.cancel') }}</button>
          <button type="submit" class="btn btn-warning"><i class="far fa-trash-alt fa-fw"></i>{{ __('app.global.delete') }}</button>
        </div>
      </form>
	  </div>
	</div>
</div>
