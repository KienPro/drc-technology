<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <img id="img-upload" class="profile-user-img img-fluid mb-1" :src="data.image ? data.image : (data.media ? data.media.url : '{{ asset('adminlte/dist/img/placeholder/slideshow_placeholder.png') }}')" style="width: 768px; height: 200px">
      <input type='file' id="img-input" name="image" class="hide-file-name" accept=".jpg,.png"/>
      <input class="btn-upload btn btn-primary form-control" type="button" value="Browse" onclick="document.getElementById('img-input').value='';document.getElementById('img-input').click();" style="width: 768px">
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>{{ __('app.slideshows.title') }}</label>
      <input type="text" name="title" class="form-control" v-model="data.title" placeholder="Enter title"/> 
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>URL</label>
      <input type="text" name="url" class="form-control" v-model="data.url" placeholder="Enter url"/> 
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>{{ __('app.slideshows.product-name') }}</label>
      <input type="text" name="product_name" class="form-control" v-model="data.product_name" placeholder="Enter product name"/> 
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>{{ __('app.slideshows.button') }}</label>
      <input type="text" name="button_name" class="form-control" v-model="data.button_name" placeholder="Enter button name"/> 
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Tagline</label>
      <input type="text" name="tag_line" class="form-control" v-model="data.tag_line" placeholder="Enter tagline"/> 
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>{{ __('app.slideshows.sequence') }}</label>
      <input type="number" name="sequence" class="form-control" v-model="data.sequence"/> 
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label class="inline-checkbox"><input type="checkbox" name="enable_status" v-model="data.enable_status"> Active</label>
    </div>
  </div>
</div>