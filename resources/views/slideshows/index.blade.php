@extends('layouts.master')
@section('content-header')
  {!! generateContentHeader('Slideshows','Slideshows') !!}
@endsection
@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="card-tools">
              @if(checkUserPermission($user_per, 'slideshow-create'))
              <a href="{{ route('slideshows.create') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus fa-fw"></i>{{ __('app.global.add-new') }}</a>
              @endif
            </div>
          </div>
          <div class="card-body">
            @include('slideshows.table')    
          </div>
          <div class="card-footer clearfix">
             @include('layouts.pagination') 
          </div>
        </div>
      </div>
    </div>
  </div>    
@endsection