@include('layouts.master_header')

<body class="hold-transition login-page" style="background-color:#fff">
    <div class="text-center mb-2">
        <a href="#" class="h1"><img src="{{ asset('adminlte/dist/img/drc-transparent-logo.png') }}" alt="drc-tranparent-log.png" width="200" class="m-3"></a>
    </div>
    
    @yield('content')
</body>

@include('layouts.master_footer')