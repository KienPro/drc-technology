<li class="nav-item dropdown user-menu">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
      <img src="{{ $auth->user->media->url ?? url('adminlte/dist/img/placeholder/square_avatar_placeholder.jpg') }}" class="user-image" alt="User Image">
      <span class="hidden-xs">{{ $auth->user->username ?? 'Unknown' }}</span>
    </a>
    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
      <!-- User image -->
      <li class="user-header bg-primary">
        <img src="{{ $auth->user->media->url ?? url('adminlte/dist/img/placeholder/square_avatar_placeholder.jpg') }}" class="img-circle elevation-2" alt="User Image">

        <p>
          {{ $auth->user->username ?? 'Unknown' }}
        </p>
      </li>
      <!-- Menu Body -->
      
      <!-- Menu Footer-->
      <li class="user-footer">
        <a href="{{ route('profile') }}" class="btn btn-default btn-flat">            
          {{ __('app.main-sidebar.profile') }}</a>
        <a href="{{ url('logout') }}" class="btn btn-default btn-flat float-right">            
          {{ __('app.main-sidebar.sign-out') }}</a>
      </li>
    </ul>
</li>