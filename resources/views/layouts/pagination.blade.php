<div class="card-title mt-2">{{ __('app.global.total') }}: {{ $data->total() }} {{ __('app.global.records') }}</div>
<div class="card-tools float-right">{{  $data->links()  }}</div>
