@php(getGeneralData($auth))
@include('layouts.master_header')
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">   
    @include('layouts.navbar')

    <aside class="main-sidebar sidebar-dark-primary elevation-2">
      <a href="#" class="brand-link text-center">
        <img src="{{ asset('adminlte/dist/img/drc-logo.jpg') }}" alt="AdminLTE Logo" class="img-circle elevation-3" style="width: 100px">
      </a>

      
      @include('layouts.sidebar')
    </aside>

    <div class="content-wrapper">
      <section class="content-header">
        <div class="container-fluid">
          @yield('content-header')             
        </div>
      </section>
      
      <section class="content">
        @yield('content')
      </section>
    </div>

    <footer class="main-footer">
      <strong>POWERED BY <a href="#">SK Team</a> &copy; 2021-2022 .</strong>
      All rights reserved.
      <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.0
      </div>
    </footer>

    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
  </div>
</body>
@include('layouts.master_footer')
@include('layouts.alert')