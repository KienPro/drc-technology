<nav class="main-header navbar navbar-expand navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button" data-enable-remember="true"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item" title="Dark Mode">
        <div class="theme-switch-wrapper nav-link">
          <label class="theme-switch" for="checkbox">
            <input type="checkbox" id="checkbox">
            <span class="slider round"></span>
          </label>
        </div>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" href="{{ route('view-orders'). "?status=pending" }}" title="Pending Order">
          <i class="far fa-bell"></i>
          <span class="badge badge-danger navbar-badge">{{ $count_order == 0 ? '' : $count_order }}</span>
        </a>
      </li>
      <!-- Multiple Language -->
      <li class="dropdown">
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">
          <i class="far fa-globe fa-fw" style="size: 40px;">
          </i><span class="caret"></span></a>
        <ul class="dropdown-menu border-1 shadow">
            <li>
                <a class="dropdown-item" rel="alternate" hreflang="{{ 'en' }}" href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}">
                    <img src="{{asset('adminlte/dist/img/lang_flag/english.png')}}" style="width: 20px; margin:5px;"> 
                    {{ 'English' }}
                </a>
            </li>
            <li>
                <a class="dropdown-item" rel="alternate" hreflang="{{ 'km' }}" href="{{ LaravelLocalization::getLocalizedURL('km', null, [], true) }}" >
                    <img src="{{asset('adminlte/dist/img/lang_flag/khmer.png')}}" style="width: 20px; margin:5px;"> 
                    {{ 'ភាសាខ្មែរ' }}
                </a>
            </li>
            
        </ul>
    </li>
      @include('layouts.user_profile')
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>
  </nav>