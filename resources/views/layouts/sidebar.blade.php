<div class="sidebar" style="height: calc(85vh - (1.5rem + 1px));">
  <!-- SidebarSearch Form -->
  <div class="form-inline mt-3">
    <div class="input-group" data-widget="sidebar-search">
      <input class="form-control form-control-sidebar" type="search" placeholder="{{ __('app.main-sidebar.search') }}" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-sidebar">
          <i class="fas fa-search fa-fw"></i>
        </button>
      </div>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      @if(checkUserPermission($user_per, 'dashboard-read'))
      <li class="nav-item">
        <a href="/" class="nav-link {{ request()->is('*admin') ? 'active' : '' }}">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            {{ __('app.main-sidebar.dashboard') }}
          </p>
        </a>
      </li>
      @endif
      @if(checkUserNeedOnePermission($user_per, ['order-read', 'product-read']))
        <li class="nav-header"><strong>{{ __('app.main-sidebar.manage') }}</strong></li>
        @if(checkUserPermission($user_per, 'order-read'))
          <li class="nav-item" >
            <a href="/admin/view-orders" class="nav-link {{ request()->is('*admin/view-orders') ? 'active' : '' }}">
              <i class="nav-icon far fa-eye"></i>
              <p>
                {{ __('app.main-sidebar.view-order') }}
              </p>
            </a>
          </li>
        @endif
        @if(checkUserPermission($user_per, 'product-read'))
        <li class="nav-item">
          <a href="/admin/products" class="nav-link {{ request()->is(['*admin/products*','*admin/product_image*']) ? 'active' : '' }}">
            <i class="nav-icon far fa-box-full"></i>
            <p>
              {{ __('app.main-sidebar.product') }}
            </p>
          </a>
        </li>
        @endif
      @endif
      @if(checkUserNeedOnePermission($user_per, [ 'customer_report-read','payment_report-read']))
        <li class="nav-header"><strong>{{ __('app.main-sidebar.reports') }}</strong></li>
        {{-- @if(checkUserPermission($user_per, 'sale-read'))
        <li class="nav-item" >
          <a href="/admin/reports/sale_report" class="nav-link {{ request()->is('*admin/reports/sale_report*') ? 'active' : '' }}">
            <i class="nav-icon far fa-file-chart-line"></i>
            <p>
              {{ __('app.main-sidebar.sale-report') }}
            </p>
          </a>
        </li>
        @endif --}}
      @endif
      @if(checkUserPermission($user_per, 'customer_report-read'))
      <li class="nav-item" >
        <a href="/admin/reports/customer_report" class="nav-link {{ request()->is('*admin/reports/customer*') ? 'active' : '' }}">
          <i class="nav-icon far fa-file-chart-line"></i>
          <p>
            {{ __('app.main-sidebar.customer-report') }}
          </p>
        </a>
      </li>
      @endif
      @if(checkUserPermission($user_per, 'payment_report-read'))
      <li class="nav-item" >
        <a href="/admin/reports/payment_report" class="nav-link {{ request()->is('*admin/reports/payment*') ? 'active' : '' }}">
          <i class="nav-icon far fa-file-chart-line"></i>
          <p>
            {{ __('app.main-sidebar.payment-report') }}
          </p>
        </a>
      </li>
      @endif
      @if(checkUserNeedOnePermission($user_per, ['product_category-read', 'product_model-read','product_tag-read','slideshow-read','role-read','user-read' 
      ]))
        <li class="nav-header"><strong>{{ __('app.main-sidebar.setting') }}</strong></li>
        @if(checkUserPermission($user_per, 'product_category-read'))
        <li class="nav-item" >
          <a href="/admin/product-categories" class="nav-link {{ request()->is('*admin/product-categories*') ? 'active' : '' }}">
            <i class="nav-icon far fa-boxes"></i>
            <p>
              {{ __('app.main-sidebar.product-category') }}
            </p>
          </a>
        </li>
        @endif
        @if(checkUserPermission($user_per, 'product_model-read'))
        <li class="nav-item" >
          <a href="/admin/product-models" class="nav-link {{ request()->is('*admin/product-models*') ? 'active' : '' }}">
            <i class="nav-icon far fa-laptop"></i>
            <p>
              {{ __('app.main-sidebar.product-model') }}
            </p>
          </a>
        </li>
        @endif
        @if(checkUserPermission($user_per, 'product_tag-read'))
        <li class="nav-item" >
          <a href="/admin/product-tags" class="nav-link {{ request()->is('*admin/product-tags*') ? 'active' : '' }}">
            <i class="nav-icon far fa-tags"></i>
            <p>
              {{ __('app.main-sidebar.product-tags') }}
            </p>
          </a>
        </li>
        @endif
        @if(checkUserPermission($user_per, 'slideshow-read'))
        <li class="nav-item" >
          <a href="/admin/slideshows" class="nav-link {{ request()->is('*admin/slideshows*') ? 'active' : '' }}">
            <i class="nav-icon far fa-images"></i>
            <p>
              {{ __('app.main-sidebar.slideshow') }}
            </p>
          </a>
        </li>
        @endif
        @if(checkUserPermission($user_per, 'role-read'))
        <li class="nav-item" >
          <a href="/admin/roles" class="nav-link {{ request()->is('*admin/roles*') ? 'active' : '' }}">
            <i class="nav-icon far fa-sitemap"></i>
            <p>
              {{ __('app.main-sidebar.role') }}
            </p>
          </a>
        </li>
        @endif
        @if(checkUserPermission($user_per, 'user-read'))
        <li class="nav-item" >
          <a href="/admin/users" class="nav-link {{ request()->is('*admin/users*') ? 'active' : '' }}">
            <i class="nav-icon far fa-user fa-fw"></i>
            <p>
              {{ __('app.main-sidebar.user') }}
            </p>
          </a>
        </li>
        @endif
      </li>
      @endif
    </ul>
  </nav>
</div>
