<div class="row">
  <div class="col-md-6">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <img id="img-upload" class="profile-user-img img-fluid img-circle mb-1" :src="data.image ? data.image : (data.media ? data.media.url : '{{ asset('adminlte/dist/img/placeholder/square_avatar_placeholder.jpg') }}')" style="width: 120px; height: 120px">
          <input type='file' id="img-input" name="image" class="hide-file-name" accept=".jpg,.png"/>
          <input class="btn-upload btn btn-primary form-control" type="button" value="Browse" onclick="document.getElementById('img-input').value='';document.getElementById('img-input').click();" style="width: 120px;">
        </div>
      </div>
      <div class="col-md-9">
      <div class="form-group">
          <label>{{ __('app.profile.first-name') }}</label>
          <input type="text" class="form-control" name="first_name" placeholder="Enter firstname" v-model="data.first_name">
      </div>
      <div class="form-group">
          <label>{{ __('app.profile.last-name') }}</label>
          <input type="text" class="form-control" name="last_name" v-model="data.last_name" placeholder="Enter lastname" v-validate="'required'">
      </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label class="required">{{ __('app.profile.username') }}</label>
          <input type="text" class="form-control" name="username" v-model="data.username" placeholder="Enter username" v-validate="'required'">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label class="required">{{ __('app.profile.email') }}</label>
          <input type="email" class="form-control" name="email" v-model="data.email" readonly>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label class="required">{{ __('app.profile.gender') }}</label>
          <select class="form-control gender-select2" name="gender" v-select2 v-model="data.gender" v-validate="'required'" style="width: 100%;">
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label class="required">{{ __('app.profile.phone-number') }}</label>
          <input type="text" class="form-control" name="phone_number" v-model="data.phone_number" v-validate="'required'" placeholder="Enter phone number">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label>{{ __('app.profile.address') }}</label>
          <textarea name="address" class="form-control" rows="2" placeholder="Type something..." v-model="data.address"></textarea>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>{{ __('app.profile.description') }}</label>
      <textarea name="description" class="form-control" rows="17" placeholder="Type something..." v-model="data.description"></textarea>
    </div>
  </div>
</div>


