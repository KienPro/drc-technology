@extends('layouts.master')
@section('content-header')
{!! generateContentHeader('Profile','Profile','Edit') !!}
<div class="button mt-2 ml-2">
  <button type="submit" form="editProfile" class="btn btn-primary"><i class="fas fa-save fa-fw"></i>{{ __('app.profile.save-changes') }}</button>
  <button class="btn btn-default" data-toggle="modal" data-target="#modal-change-password"><i class="fas fa-lock fa-fw"></i>{{ __('app.profile.change-password') }}</button>
</div>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title text-info"><i class="far fa-info-circle fa-fw"></i>{{ __('app.profile.profile-information') }}</h3>          
        </div>
        <div class="card-body">
          <form @submit.prevent="submit" id="editProfile" v-cloak>
            @include('profile.form')  
        </form>     
        </div>
      </div>
    </div>
  </div>
</div>
@include('profile.modal-crop-image')
@include('profile.password.index')
@endsection
@section('footer-content')
  <script>
    const data = <?php echo json_encode($data); ?>;
    let editProfile;
  </script>
  <script src="{{ mix('dist/js/app.js') }}"></script>
  <script src="{{ mix('dist/js/profile.js') }}"></script>
  <script>
    var image_crop = $('#image-crop').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport:{
          width: 300, 
          height: 300, 
          type: 'circle'
        },
        boundary:{
          width: 400, 
          height: 400}
    });    

    $('#img-input').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
            image_crop.croppie('bind', {
                url: event.target.result,
            })
        }
        reader.readAsDataURL(this.files[0]);
        $('#modal-crop-image').modal('show');
    });
   
    $('.submit-crop').click(function(){
        image_crop.croppie('result', {
            type: 'base64',
            size: {width:1080, height:1080},
        }).then(function(res){
            $('#modal-crop-image').modal('hide');
            $('#img-upload').attr('src', res);
            editProfile.data.image = res
        })
    })
  
    $('.gender-select2').select2({
        //this code use for hide search box in select2
        minimumResultsForSearch:-1
    })
  </script>
@endsection
