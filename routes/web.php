<?php
Route::group(['middleware' => 'auth:guest'], function () {
    Route::get('/', 'AuthController@index');
    Route::get('/login', 'AuthController@index');
    Route::post('/login', 'AuthController@login');

    Route::get('/forget', 'AuthController@forget');
    Route::post('/forget', 'AuthController@submitforgetPassword');

    Route::get('/verify/{email}', 'AuthController@verify');
    Route::post('/verify', 'AuthController@submitVerify');

    Route::get('/reset/{email}/{token}', 'AuthController@reset');
    Route::post('/reset', 'AuthController@submitResetPassword');
});

Route::get('/logout', 'AuthController@logout');

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){

    Route::get('/home', 'HomeController@index')->name('home');
        
    Route::group(['prefix' => 'admin', 'middleware'=>'auth'], function () {

        // Dashboard
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::get('/dashboard/list', 'DashboardController@dashboard');


        // Profile
        Route::group(['prefix' =>'profile'], function(){
            Route::get('/', 'ProfileController@edit')->name('profile');
            Route::post('/', 'ProfileController@update')->name('profile.update');
            Route::post('/change_password', 'ProfileController@changePassword')->name('profile.password');
        });
        
        //Product Categories
        Route::prefix('product-categories')->group(function () {
            Route::get('/', 'ProductCategoryController@index')->name('product-categories')->middleware('permission:product_category-read');

            Route::get('/create', 'ProductCategoryController@create')->name('product-categories.create')->middleware('permission:product_category-create');
            Route::post('/', 'ProductCategoryController@store')->name('product-categories.store')->middleware('permission:product_category-create');

            Route::get('/{id}', 'ProductCategoryController@edit')->name('product-categories.edit')->middleware('permission:product_category-update');
            Route::post('/{id}', 'ProductCategoryController@update')->name('product-categories.update')->middleware('permission:product_category-update');

            Route::delete('/{id}', 'ProductCategoryController@destroy')->name('product-categories.destroy')->middleware('permission:product_category-delete');
        });
        
        // Product Models
        Route::prefix('product-models')->group(function () {
            Route::get('/', 'ProductModelController@index' )->name('product-models')->middleware('permission:product_model-read');

            Route::get('/create', 'ProductModelController@create' )->name('product-models.create')->middleware('permission:product_model-create');
            Route::post('/', 'ProductModelController@store' )->name('product-models.store')->middleware('permission:product_model-create');

            Route::get('/{id}', 'ProductModelController@edit' )->name('product-models.edit')->middleware('permission:product_model-update');
            Route::post('/{id}', 'ProductModelController@update' )->name('product-models.update')->middleware('permission:product_model-update');

            Route::delete('/{id}', 'ProductModelController@destroy' )->name('product-models.destroy')->middleware('permission:product_model-delete');
        });

        // Product Tags
        Route::prefix('product-tags')->group(function () {
            Route::get('/', 'ProductTagsController@index' )->name('product-tags')->middleware('permission:product_tag-read');

            Route::get('/create', 'ProductTagsController@create' )->name('product-tags.create')->middleware('permission:product_tag-create');
            Route::post('/', 'ProductTagsController@store' )->name('product-tags.store')->middleware('permission:product_tag-create');

            Route::get('/{id}', 'ProductTagsController@edit' )->name('product-tags.edit')->middleware('permission:product_tag-update');
            Route::post('/{id}', 'ProductTagsController@update' )->name('product-tags.update')->middleware('permission:product_tag-update');

            Route::delete('/{id}', 'ProductTagsController@destroy' )->name('product-tags.destroy')->middleware('permission:product_tag-delete');
        });

        // Roles
        Route::prefix('roles')->group(function () {
            Route::get('/', 'RoleController@index' )->name('roles')->middleware('permission:role-read');

            Route::get('/create', 'RoleController@create')->name('roles.create')->middleware('permission:role-create');
            Route::post('/', 'RoleController@store')->name('roles.store')->middleware('permission:role-create');

            Route::get('/{id}', 'RoleController@edit')->name('roles.edit')->middleware('permission:role-update');
            Route::post('/{id}', 'RoleController@update')->name('roles.update')->middleware('permission:role-update');

            Route::delete('/{id}', 'RoleController@destroy')->name('roles.destroy')->middleware('permission:role-delete');
        });

        // Users
        Route::prefix('users')->group(function () {
            Route::get('/', 'UserController@index' )->name('users')->middleware('permission:user-read');

            Route::get('/create', 'UserController@create')->name('users.create')->middleware('permission:user-create');
            Route::post('/', 'UserController@store')->name('users.store')->middleware('permission:user-create');

            Route::get('/{id}', 'UserController@edit')->name('users.edit')->middleware('permission:user-update');
            Route::put('/{id}', 'UserController@update')->name('users.update')->middleware('permission:user-update');

            Route::delete('/delete/{id}', 'UserController@destroy' )->name('users.destroy')->middleware('permission:user-update');
        });

        // Order
        Route::prefix('view-orders')->group(function () {
            Route::get('/', 'ViewOrderController@index' )->name('view-orders')->middleware('permission:order-read');
            Route::post('/{id}', 'ViewOrderController@update' )->name('view-orders.update')->middleware('permission:order-update');
            Route::get('/print/invoice/{id}', 'ViewOrderController@invoice')->name('view-orders.invoice');
            Route::post('/receive_payment/{id}', 'ViewOrderController@payment')->name('view-orders.payment')->middleware('permission:order-receive');
            Route::delete('/product/delete/{id}', 'ViewOrderController@deleteProduct')->name('view-orders.product.delete');

        });

        // Product
        Route::prefix('products')->group(function () {
            Route::get('/', 'ProductController@index' )->name('products')->middleware('permission:product-read');

            Route::get('/create', 'ProductController@create' )->name('products.create')->middleware('permission:product-create');
            Route::post('/', 'ProductController@store' )->name('products.store')->middleware('permission:product-create');

            Route::get('/{id}', 'ProductController@edit' )->name('products.edit')->middleware('permission:product-update');
            Route::post('/{id}', 'ProductController@update' )->name('products.update')->middleware('permission:product-update');

            Route::delete('/delete/{id}', 'ProductController@destroy' )->name('products.destroy')->middleware('permission:product-delete');

            Route::get('/export/excel', 'ProductController@exportExcel' )->name('products.export.excel')->middleware('permission:product-export');
            Route::post('/status/{id}', 'ProductController@bestseller')->name('products.update.bestseller');
        });

        // Product Image
        Route::prefix('product_images')->group(function () {
            Route::get('/', 'ProductImageController@index' )->name('product_images');

            Route::get('/create', 'ProductImageController@create' )->name('product_images.create');
            Route::post('/', 'ProductImageController@store')->name('product_images.store');

            Route::delete('/delete/{id}', 'ProductImageController@destroy' )->name('product_images.destroy');
        });

        // Report
        Route::prefix('reports')->group(function () {
            Route::get('/sale_report', 'ReportController@sale_report' )->name('reports.sale')->middleware('permission:sale-read');

            // Route::get('/payment_report', 'ReportController@payment_report' )->name('reports.payment')->middleware('permission:payment-read');
            Route::get('/payment_report', 'ReportController@payment_report' )->name('reports.payment')->middleware('permission:payment_report-read');
            Route::get('/export/payment', 'ReportController@exportPaymentExcel' )->name('export.payment')->middleware('permission:payment_report-export');

            Route::get('/customer_report', 'ReportController@customer_report' )->name('reports.customer')->middleware('permission:customer_report-read');
            Route::get('/export/customer', 'ReportController@exportCustomerExcel' )->name('export.customer')->middleware('permission:customer_report-export');

        });

        // Slideshow
        Route::prefix('slideshows')->group(function () {
            Route::get('/', 'SlideshowController@index' )->name('slideshows')->middleware('permission:slideshow-read');

            Route::get('/create', 'SlideshowController@create' )->name('slideshows.create')->middleware('permission:slideshow-create');
            Route::post('/', 'SlideshowController@store')->name('slideshows.store')->middleware('permission:slideshow-create');

            Route::get('/{id}', 'SlideshowController@edit')->name('slideshows.edit')->middleware('permission:slideshow-update');
            Route::post('/{id}', 'SlideshowController@update')->name('slideshows.update')->middleware('permission:slideshow-update');
            
            Route::delete('/delete/{id}', 'SlideshowController@destroy' )->name('slideshows.destroy')->middleware('permission:slideshow-delete');
        });
    });
});


