<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\ProductsExport;

class ProductController extends Controller
{
    public function index()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/products/list',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/products'),
            $current_page,[
                'product_category_id' => request('product_category_id'),
                'product_model_id' => request('product_model_id'),
                'is_new' => request('is_new'),
                'is_bestseller' => request('is_bestseller'),
                'enable_status' => request('enable_status'),
                'search'=> request('search')
            ]
        );
        $response_product_categories = $this->api_get('admin/product_categories/list_all');
        $response_product_models = $this->api_get('admin/product_models/list_all');
        $response_product_tags = $this->api_get('admin/product_tags/list_all');

        $product_categories = [];
        if($response_product_categories->data){
            $product_categories = $response_product_categories->data;
        }

        $product_models = [];
        if($response_product_models->data){
            $product_models = $response_product_models->data;
        }

        $product_tags = [];
        if($response_product_tags->data){
            $product_tags = $response_product_tags->data;
        }
        
        return view('products.index', compact('data','product_models','product_categories', 'product_tags'));
    }

    public function create()
    {
        $response_product_categories = $this->api_get('admin/product_categories/list_all');
        $response_product_models = $this->api_get('admin/product_models/list_all');
        $response_product_tags = $this->api_get('admin/product_tags/list_all');

        $product_categories = [];
        if($response_product_categories->data){
            $product_categories = $response_product_categories->data;
        }

        $product_models = [];
        if($response_product_models->data){
            $product_models = $response_product_models->data;
        }

        $product_tags = [];
        if($response_product_tags->data){
            $product_tags = $response_product_tags->data;
        }

        return view('products.create', compact('product_categories', 'product_models', 'product_tags'));
    }

    public function store(Request $request)
    {
        $result = $this->api_post('admin/products/create', $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.create_success', ['name' => 'product']));
            return ok('');
        } else {
            return fail($result->message, 200);
        } 
    }

    public function edit($id)
    {
        $data = $this->isPageNotFound('admin/products/show/'. $id);
        $response_product_categories = $this->api_get('admin/product_categories/list_all');
        $response_product_models = $this->api_get('admin/product_models/list_all');
        $response_product_tags = $this->api_get('admin/product_tags/list_all');

        $product_categories = [];
        if($response_product_categories->data){
            $product_categories = $response_product_categories->data;
        }

        $product_models = [];
        if($response_product_models->data){
            $product_models = $response_product_models->data;
        }

        $product_tags = [];
        if($response_product_tags->data){
            $product_tags = $response_product_tags->data;
        }
    
        return view('products.edit', compact('data', 'product_categories', 'product_models', 'product_tags'));
    }

    public function update(Request $request, $id)
    {
        $result = $this->api_post('admin/products/update/'. $id, $request->all());

        if ($result->success == true) {
            session()->put('success', __('dialog_box.update_success', ['name' => 'product']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }

    public function destroy($id)
    {
        try {
            $result = $this->api_post('admin/products/delete/'. $id);

            if ($result->success == false) {
                $msg = self::getErrorMessage($result->message);
                return back()->with('error', $msg);
            }

            return back()->with('success', __('dialog_box.delete_success', ['name' => 'product']));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function print()
    {
       $response = $this->api_get('admin/products/list?export=true&product_category_id=' . request('product_category_id') . '&product_model_id=' . request('product_model_id') . '&search=' . request('search') . '&is_new=' . request('is_new') . '&enable_status=' . request('enable_status'));
       $data = [];
       if ($response->data) {
           $data = $response->data->list;
       }

        return view('products.print', compact('data'));
    }

    public function exportExcel()
    {
        // Get data from api
        $response = $this->api_get('admin/products/list?export=true&product_category_id=' . request('product_category_id') . '&product_model_id=' . request('product_model_id') . '&search=' . request('search') . '&is_new=' . request('is_new') . '&enable_status=' . request('enable_status'). '&is_bestseller=' . request('is_bestseller'));
        $data = [];
        if ($response->data) {
            $data = $response->data;
        }
        // Create column in excel
        $data_filter[0] = [
            '#' => 'No',
            'Product Name' => 'Product Name',
            'Category' => 'Category',
            'Model' => 'Model',
            'Type' => 'Type',
            'Tags' => 'Tags',
            'Specification' => 'Specification',
            'Old Price' => 'Old Price',
            'Current Price' => 'Current Price',
            'Bestseller' => 'Bestseller',
        ];

        // Generate row in excel by data from api
        foreach($data->list as $key => $value)
        {
            $data_filter[$key+1] = [
                '#' => ($key + 1),
                'Product Name' => $value->name,
                'Category' => $value->product_category->name,
                'Model' => $value->product_model->name,
                'Type' => $value->is_new ? 'New' : 'Used',
                'Tags' => $value->string_tags,
                'Specification' => strip_tags(htmlspecialchars_decode($value->specification, ENT_NOQUOTES)),
                'Old Price' => $value->old_price == 0 ? "0" : $value->old_price ,
                'Current Price' => $value->current_price == 0 ? "0" : $value->current_price ,
                'Bestseller' => $value->is_bestseller == 1 ? "Yes" : "No",
            ];
        }
        // Export data
        $export = new ProductsExport(
            $data_filter
        );
        // Download
        return Excel::download($export, 'Products.xlsx');
    }

    public function bestseller(Request $request, $id)
    {
        try {
            $result = $this->api_post('admin/products/status/bestseller/'. $id, $request->only('is_bestseller'));
            if ($result->success == false) {
                $msg = self::getErrorMessage($result->message);
                return back()->with('error', $msg);
            }

            return back()->with('success', __('dialog_box.update_success', ['name' => 'product']));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
