<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function edit()
    {
            $response = $this->api_get('admin/auth/profile');
            $data = null;
            if ($response->data) {
                $data = $response->data;
            }
        return view('profile.edit', compact('data'));
    }

    public function update(Request $request)
    {        
        $result = $this->api_post('admin/auth/profile', $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.update_success', ['name' => 'Profile']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }

    public function changePassword(Request $request)
    {
        $result = $this->api_post('admin/auth/profile/password', $request->all());

        if ($result->success == true) {
            session()->put('success', __('dialog_box.update_success', ['name' => 'Change password']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }
}
