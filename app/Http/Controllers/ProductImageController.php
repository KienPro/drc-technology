<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductImageController extends Controller
{
    public function index()
    {
        $product_id = request('product_id');
        $response = $this->api_get('admin/product_images/list?product_id='. $product_id);

        $product_images = [];
        if($response->data->list){
            $product_images = $response->data->list;
        }
        return view('products.images.index', compact('product_images'));
    }

    public function create()
    {
        return view('products.images.create');
    }

    public function store(Request $request)
    {
        $result = $this->api_post('admin/product_images/create', $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.create_success', ['name' => 'upload product image']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }

    public function destroy($id)
    {
        try {
            $result = $this->api_post('admin/product_images/delete/'. $id);

            if ($result->success == false) {
                $msg = self::getErrorMessage($result->message);
                return back()->with('error', $msg);
            }

            return back()->with('success', __('dialog_box.delete_success', ['name' => 'product image']));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }


}
