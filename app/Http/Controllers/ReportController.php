<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\ReportCustomerExport;
use App\Exports\ReportPaymentExport;
class ReportController extends Controller
{
    public function sale_reports()
    {
        return view('reports.sale-reports.index');
    }

    public function customer_report()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/reports/customer',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/customer_reports'),
            $current_page,
            [
                'status' => request('status'),
                'from_date' => request('from_date'),
                'to_date' => request('to_date'),
            ]
        );

        return view('reports.customer-report.index', compact('data'));
    }

    public function exportCustomerExcel()
    {
        // Get data from api
        $response = $this->api_get('admin/reports/customer?export=true&from_date=' . request('from_date') . '&to_date=' . request('to_date'). '&search='.request('search'));
        $data = [];
        if ($response->data) {
            $data = $response->data;
        }
        // Create column in excel
        $data_filter[0] = [
            '#' => 'No',
            'Customer Name' => 'Customer Name',
            'Email' => 'Email',
            'Phone Number' => 'Phone Number',
            'Registerd Date' => 'Registered Date',
        ];

        // Generate row in excel by data from api
        foreach($data->list as $key => $value)
        {
            $data_filter[$key+1] = [
                '#' => ($key + 1),
                'Customer Name' => $value->name,
                'Email' => $value->email,
                'Phone Number' => $value->phone_number,
                'Registerd Date' => $value->created_at,
            ];
        }
        // Export data
        $export = new ReportCustomerExport(
            $data_filter
        );
        // Download
        return Excel::download($export, 'Customer-Report.xlsx');
    }


    public function payment_report()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/reports/payment',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/payment_reports'),
            $current_page,
            [
                'from_date' => request('from_date'),
                'to_date' => request('to_date'),
            ]
        );

        return view('reports.payment-report.index', compact('data'));
    }

     public function exportPaymentExcel()
    {
        // Get data from api
        $response = $this->api_get('admin/reports/payment?export=true&search='.request('search'));
        $data = [];
        if ($response->data) {
            $data = $response->data;
        }
        // Create column in excel
        $data_filter[0] = [
            '#' => 'No',
            'Order Code' => 'Order Code',
            'Customer Name' => 'Customer Name',
            'Email' => 'Email',
            'Phone Number' => 'Phone Number',
            'Total Amount' => 'Total Payment',
            'Balance' => 'Balance',
            'Status' => 'Status',
        ];
        // Generate row in excel by data from api
        foreach($data->list as $key => $value)
        {
            $data_filter[$key+1] = [
                '#' => ($key + 1),
                'Order Code' => "#".sprintf("%'.06d", $value->order->code),
                'Customer Name' => $value->order->customer->name,
                'Email' => $value->order->customer->email,
                'Phone Number' => $value->order->customer->phone_number,
                'Total Amount' => $value->order->total == 0 ? '0' : $value->order->total,
                'Balance' => $value->balance == 0 ? '0' : $value->balance,
                'Status' => $value->balance >= $value->order->total ? 'Closed' : 'Open',
            ];
        }
        // Export data
        $export = new ReportPaymentExport(
            $data_filter
        );
        // Download
        return Excel::download($export, 'Payment-Report.xlsx');
    }
    
    public function sale_report()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/reports/sale',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/sale_reports'),
            $current_page,
            [
                'status' => request('status'),
                'from_date' => request('from_date'),
                'to_date' => request('to_date'),
            ]
        );

        return view('reports.sale-report.index', compact('data'));
    }
}
