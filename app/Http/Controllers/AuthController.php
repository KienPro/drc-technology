<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {       
        $result = $this->api_post('admin/auth/login', $request->all());
        if ($result->success == false) {
            return back()->withInput(request()->except('password'))->with('error', self::getErrorMessage($result->message));
        }
        
        session()->put('auth', $result->data);
        return redirect()->to('/admin');
    }

    public function logout()
    {
        return self::clearAuth();
    }

    public function forget()
    {
        return view('auth.passwords.forget');
    }

    public function submitForgetPassword(Request $request)
    {
        $result = $this->api_post('admin/auth/forgot_password', $request->all());

        if ($result->success == false) {
            return back()->withInput()->with('error', self::getErrorMessage($result->message));
        }

        return redirect()->to('/verify/'.$request->email);
    }

    public function verify($email)
    {
        return view('auth.passwords.verify', compact('email'));
    }

    public function submitVerify(Request $request)
    {
        $result = $this->api_post('admin/auth/forgot_password/verify',$request->all());

        if ($result->success == false) {
            return back()->withInput()->with('error', self::getErrorMessage($result->message));
        }
        $email = $request->email;
        $token = $result->data->token;

        return redirect()->to('/reset/'.$email.'/'.$token);
    }

    public function reset($email, $token)
    {
        return view('auth.passwords.reset', compact('email', 'token'));
    }

    public function submitResetPassword(Request $request)
    {
        $password = $request->password;
        $confirm_password = $request->confirm_password;
        if($password == $confirm_password && $password!=null&$confirm_password!=null){
            $result = $this->api_post('admin/auth/forgot_password/reset', $request->all());  
            if ($result->success == false) {
                return back()->withInput()->with('error', self::getErrorMessage($result->message));
            }
            return redirect()->to('/login')->with('message', 'Password Reset Successfully');
        }elseif($password != $confirm_password){
            return back()->withInput()->with('error', self::getErrorMessage("Password and confirm password not match!"));
        }else{
            return back()->withInput()->with('error', self::getErrorMessage("Password and confirm password invalid!"));
        }

    }
    
}
