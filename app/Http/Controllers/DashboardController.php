<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    
    public function index()
    {
        return view('dashboard');
    }

    public function dashboard()
    {
        $res = $this->api_get('admin/dashboard');

        if(!$res->success) 
            return fail($res->message);
        return ok($res->data);
    }
}
