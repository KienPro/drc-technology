<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/users/list',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/users'),
            $current_page
        );
        return view('users.index', compact('data'));
    }

    public function create()
    {
        $response = $this->api_get('admin/roles/list/all');
        $roles = [];
        if ($response->data) {
            $roles = $response->data;
        }
        return view('users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $result = $this->api_post('admin/users/create', $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.create_success', ['name' => 'User']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }

    public function edit($id)
    {
        $data = $this->isPageNotFound('admin/users/show/' . $id);
        $response = $this->api_get('admin/roles/list/all');
        $roles = [];
        if ($response->data) {
            $roles = $response->data;
        }
        return view('users.edit', compact('data', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $result = $this->api_post('admin/users/update/'. $id, $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.update_success', ['name' => 'User']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
        return redirect()->route('users')->with('success','User updated successfully');
    }

    public function destroy($id)
    {
        try {
            $result = $this->api_post('admin/users/delete/'. $id);
            if ($result->success == false) {
                $msg = self::getErrorMessage($result->message);
                return back()->with('error', $msg);
            }

            return back()->with('success', __('dialog_box.delete_success', ['name' => 'User']));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
        // return redirect()->route('users')->with('success','User deleted successfully');
    }
}
