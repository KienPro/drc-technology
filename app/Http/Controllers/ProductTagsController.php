<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductTagsController extends Controller
{

    public function index()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/product_tags/list',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/product-tags'),
            $current_page,
        );

        return view("product_tags.index", compact('data'));
    }

    public function create()
    {
        return view("product_tags.create");
    }

    public function store(Request $request)
    {
        $result = $this->api_post('admin/product_tags/create', $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.create_success', ['name' => 'product tags']));
            return ok('');
        } else {
            return fail($result->message, 200);
        } 
    }

    public function edit($id){
        $data = $this->isPageNotFound('admin/product_tags/show/'. $id);
    
        return view('product_tags.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $result = $this->api_post('admin/product_tags/update/'. $id, $request->all());

        if ($result->success == true) {
            session()->put('success', __('dialog_box.update_success', ['name' => 'product tags']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }

    public function destroy($id){
        try {
            $result = $this->api_post('admin/product_tags/delete/'. $id);

            if ($result->success == false) {
                $msg = self::getErrorMessage($result->message);
                return back()->with('error', $msg);
            }

            return back()->with('success', __('dialog_box.delete_success', ['name' => 'product tag']));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
