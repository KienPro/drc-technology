<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewOrderController extends Controller
{
    public function index()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/view_orders/list',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/view-orders'),
            $current_page,
            [
                'status' => request('status'),
                'search' => request('search'),
                'from_date' => request('from_date'),
                'to_date' => request('to_date')
            ]
        );
        return view('view_orders.index', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $result = $this->api_post('admin/view_orders/update/'. $id, $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.update_success', ['name' => 'order']));

            return redirect()->route('view-orders')->with('success','Order updated successfully');
        } else {
            return fail($result->message, 200);
        }
    }

    public function invoice($code)
    {
        $data = $this->api_get('admin/view_orders/invoice/'. $code);
        
        return view('view_orders.tab_order.print', compact('data'));
    }

    public function payment(Request $request, $id)
    {
        $result = $this->api_post('admin/view_orders/payment/'. $id, $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.create_success', ['name' => 'order']));

            return ok();
        } else {
            return fail($result->message, 200);
        }
    }

    public function deleteProduct($id)
    {

        try {
            $result = $this->api_post('admin/view_orders/product/delete/'. $id);
            
            if ($result->success == false) {
                $msg = self::getErrorMessage($result->message);
                return back()->with('error', $msg);
            }

            return back()->with('success', __('dialog_box.delete_success', ['name' => 'product']));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
