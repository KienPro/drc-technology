<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductModelController extends Controller
{
    public function index()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/product_models/list',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/product-models'),
            $current_page,
        );

        return view("product_models.index", compact('data'));
    }

    public function create()
    {
        $response_product_categories = $this->api_get('admin/product_categories/list_all');

        $product_categories = [];
        if($response_product_categories->data){
            $product_categories = $response_product_categories->data;
        }

        return view("product_models.create", compact('product_categories'));
    }

    public function store(Request $request)
    {
        $result = $this->api_post('admin/product_models/create', $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.create_success', ['name' => 'product model']));
            return ok('');
        } else {
            return fail($result->message, 200);
        } 
    }

    public function edit($id)
    {
        $data = $this->isPageNotFound('admin/product_models/show/'. $id);
        $response_product_categories = $this->api_get('admin/product_categories/list_all');

        $product_categories = [];
        if($response_product_categories->data){
            $product_categories = $response_product_categories->data;
        }
    
        return view('product_models.edit', compact('data', 'product_categories'));
    }

    public function update(Request $request, $id)
    {
        $result = $this->api_post('admin/product_models/update/'. $id, $request->all());

        if ($result->success == true) {
            session()->put('success', __('dialog_box.update_success', ['name' => 'product model']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }

    public function destroy($id)
    {
        try {
            $result = $this->api_post('admin/product_models/delete/'. $id);

            if ($result->success == false) {
                $msg = self::getErrorMessage($result->message);
                return back()->with('error', $msg);
            }

            return back()->with('success', __('dialog_box.delete_success', ['name' => 'product model']));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
