<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SlideshowController extends Controller
{
    public function index()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/slideshows/list',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/slideshows'),
            $current_page
        );
        return view('slideshows.index', compact('data'));
    }

    public function create()
    {
        return view('slideshows.create');
    }

    public function store(Request $request)
    {
        $result = $this->api_post('admin/slideshows/create', $request->all());
        if ($result->success == true) {
            session()->put('success', __('dialog_box.create_success', ['name' => 'slideshow']));
            return ok('');
        } else {
            return fail($result->message, 200);
        } 
    }

    public function edit($id)
    {
        $data = $this->isPageNotFound('admin/slideshows/show/'. $id);
    
        return view('slideshows.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $result = $this->api_post('admin/slideshows/update/'. $id, $request->all());

        if ($result->success == true) {
            session()->put('success', __('dialog_box.update_success', ['name' => 'slideshow']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }

    public function destroy($id)
    {
        try {
            $result = $this->api_post('admin/slideshows/delete/'. $id);

            if ($result->success == false) {
                $msg = self::getErrorMessage($result->message);
                return back()->with('error', $msg);
            }

            return back()->with('success', __('dialog_box.delete_success', ['name' => 'slideshow']));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
