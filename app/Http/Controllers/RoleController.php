<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'admin/roles/list',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('admin/roles'),
            $current_page
        );
        return view('roles.index', compact('data'));
    }

    public function create()
    {
        $response = $this->api_get('admin/permissions/list_all');

        $permissions = [];
        if($response->success) {
            $permissions = $response->data;
        }

        return view('roles.create', compact('permissions'));
    }

    public function edit($role_id)
    {
        $data = $this->isPageNotFound('admin/roles/show/'.$role_id);

        $response = $this->api_get('admin/permissions/list_all');

        $permissions = [];
        if($response->success) {
            $permissions = $response->data;
        }

        return view('roles.edit', compact('permissions', 'data'));
    }

    public function store(Request $request)
    {
        $result = $this->api_post('admin/roles/create', $request->all());

        if ($result->success == true) {
            session()->put('success', __('dialog_box.create_success', ['name' => 'Role']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }

    public function update($id, Request $request)
    {
        $result = $this->api_post('admin/roles/update/'. $id, $request->all());

        if ($result->success == true) {
            session()->put('success', __('dialog_box.update_success', ['name' => 'Role']));

            return ok('');
        } else {
            return fail($result->message, 200);
        }
    }

    public function updateStatus(Request $request, $index)
    {
        try {
            $data = $this->api_post('admin/role/status/' . $index, $request->only('enable_status'));

            if (!$data->success) {
                return back()->withInput()->with('error', $data->message);
            }

            return back()->with('success', __('dialog_box.update_success', ['name' => 'Role']));
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }

    public function destroy($id)
    {
        try {
            $result = $this->api_post('admin/roles/delete/'. $id);

            if ($result->success == false) {
                $msg = self::getErrorMessage($result->message);
                return back()->with('error', $msg);
            }

            return back()->with('success', __('dialog_box.delete_success', ['name' => 'Role']));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
