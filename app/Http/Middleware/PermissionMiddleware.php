<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use Closure;

class PermissionMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @param $route_permissions
     * @return \Illuminate\Http\Response|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle($request, Closure $next, $route_permissions)
    {
        $user_per = [];
        $url = 'admin/auth/user_per';

        $controller = new Controller();
        $auth = session()->get('auth');
        $controller->setAuth($auth);

        $response = $controller->api_get($url);

        if ($response->success) {
            $user_per = (array) $response->data;
        } else {
            if(isset($response->data) && $response->data == 401) {
                return redirect()->to('logout');
            }
        }

        if(!checkUserNeedAllPermission($user_per, explode("/", $route_permissions))){
            abort(403);
        }

        $request->attributes->add(['user_per' => $user_per]);

        return $next($request);
    }
}
