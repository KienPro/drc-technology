<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromArray;

class ReportPaymentExport implements FromArray
{
   protected $customers;

    public function __construct(array $customers)
    {
        $this->customer = $customers;
    }

    public function array(): array
    {
        $default_font_style = [
            'font' => ['name' => 'Arial', 'size' => 10, 'font-weight' => 'bold']
        ];
        return $this->customer;
    }
}