<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromArray;

class ProductsExport implements FromArray
{
   protected $products;

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function array(): array
    {
        $default_font_style = [
            'font' => ['name' => 'Arial', 'size' => 10, 'font-weight' => 'bold']
        ];

        return $this->products;
    }
}