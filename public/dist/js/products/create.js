/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 11);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/products/create.js":
/*!************************************************!*\
  !*** ./resources/assets/js/products/create.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

createProduct = new Vue({
  el: "#createProduct",
  data: {
    image: null,
    secondary_image: null,
    error: {
      image: null,
      secondary_image: null
    },
    data: {
      name: '',
      product_category_id: null,
      product_model_id: null,
      old_price: 0,
      current_price: 0,
      cost: 0,
      short_description: '',
      long_description: '',
      specification: '',
      sequence: 0,
      is_new: 1,
      is_bestseller: 0,
      tag_ids: [],
      enable_status: true
    },
    product_categories: product_categories,
    product_models: product_models,
    product_tags: product_tags
  },
  methods: {
    save: function save() {
      axios.post("".concat(baseURL, "/admin/products"), this.data).then(function (response) {
        if (response.data.success) {
          window.location.href = baseURL + '/admin/products';
        } else {
          showAlertError(response.data.message);
          hideLoading();
        }
      })["catch"](function (error) {
        hideLoading();
        showAlertError('Can not add product');
        console.log(error);
      });
    },
    submit: function submit() {
      var _this = this;

      showLoading();
      this.$validator.validate().then(function (result) {
        var save = true;

        if (!_this.data.image) {
          _this.error.image = 'The Image field is required.';
          save = false;
        }

        if (!_this.data.secondary_image) {
          _this.error.secondary_image = 'The secondary image field is required.';
          save = false;
        }

        if (!result || !save) {
          hideLoading(); //set Window location to top

          window.scrollTo(0, 0);
        } else {
          _this.save();
        }
      });
    },
    selectProductCategory: function selectProductCategory() {
      this.data.product_model_id = null;
      $('#product_model_id').val(null).trigger('change');
    },
    removeTag: function removeTag(removeTagId) {
      console.log(removeTagId);
      var i = 0;
      var length = this.data.tag_ids.length;

      for (i; i < length; i++) {
        if (this.data.tag_ids[i] == removeTagId) {
          this.data.tag_ids.splice(i, 1);
          break;
        }
      }
    }
  },
  computed: {
    filtered_product_models: function filtered_product_models() {
      var _this2 = this;

      if (this.data.product_category_id == null) {
        return [];
      }

      return this.product_models.filter(function (o) {
        return o.product_category_id == _this2.data.product_category_id;
      });
    }
  }
});

/***/ }),

/***/ 11:
/*!******************************************************!*\
  !*** multi ./resources/assets/js/products/create.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Thesis\drc-technology\resources\assets\js\products\create.js */"./resources/assets/js/products/create.js");


/***/ })

/******/ });