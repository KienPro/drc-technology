<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class ProductModelSeeder extends Seeder
{
    public function run()
    {
        DB::table('product_models')->insert([
            ['name' => 'MSI','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'ASUS','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'EPSON','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'HP','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'SHARP','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'BROTHER','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'SAMSUNG','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'TRANDSCAN','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'DELL','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'CANON','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'ACER','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'SONY','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'APPLE','product_category_id'=>'2','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
        ]);
    }
}
