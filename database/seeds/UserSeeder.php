<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'role_id' => 1,
                'first_name' => 'Sruo',
                'last_name' => 'Kien',
                'username' => 'KienPro',
                'email' => 'sruo.kien@gmail.com',
                'password' => Hash::make('password'),
                'gender' => 'male',
                'phone_number' => '0965704307',
                'address' => 'Doung Ngeab lll, Phnom Penh',
                'description' => 'This is description',
                'enable_status' => 1,
                'created_at' => Carbon::now()->format('Y:m:d H:i:s')
            ],
            [
                'role_id' => 2,
                'first_name' => 'Oeung',
                'last_name' => 'Kemleang',
                'username' => 'Kemleang',
                'email' => 'oeung.kemleang@gmail.com',
                'password' => Hash::make('password'),
                'gender' => 'male',
                'phone_number' => '0965557779',
                'address' => 'Olympic, Phnom Penh',
                'description' => 'This is description',
                'enable_status' => 1,
                'created_at' => Carbon::now()->format('Y:m:d H:i:s')
            ],
            [
                'role_id' => 2,
                'first_name' => 'Toum',
                'last_name' => 'Bunroath',
                'username' => 'Bunroath',
                'email' => 'bunroath5@gmail.com',
                'password' => Hash::make('password'),
                'gender' => 'male',
                'phone_number' => '010203040',
                'address' => 'Chroy Chongva, Phnom Penh',
                'description' => 'This is description',
                'enable_status' => 1,
                'created_at' => Carbon::now()->format('Y:m:d H:i:s')
            ],
            [
                'role_id' => 2,
                'first_name' => 'San',
                'last_name' => 'Monyreak',
                'username' => 'Monyreak',
                'email' => 'onyreak86@gmail.com',
                'password' => Hash::make('password'),
                'gender' => 'male',
                'phone_number' => '0887656777',
                'address' => 'Chroy Chongva, Phnom Penh',
                'description' => 'This is description',
                'enable_status' => 1,
                'created_at' => Carbon::now()->format('Y:m:d H:i:s')
            ],
        ]);
    }
}
