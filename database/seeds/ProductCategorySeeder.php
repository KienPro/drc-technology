<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class ProductCategorySeeder extends Seeder
{
    public function run()
    {
        DB::table('product_categories')->insert([
            ['name' => 'MSI','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'ASUS','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'Keyboard','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'RAM','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'Printer','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'External HardDisk','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'Monitor','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'Mouse Pad','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'Charger','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
            ['name' => 'Scanner','sequence' => '0','created_at' => Carbon::now()->format('Y:m:d H:i:s')],
        ]);
    }
}
