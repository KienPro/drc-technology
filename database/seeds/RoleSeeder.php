<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['name'=>'Admin','description'=>'This is top of user', 'enable_status'=>1, 'created_at'=>Carbon::now()->format('Y:m:d H:i:s')],
            ['name'=>'Seller','description'=>'This is seller of user', 'enable_status'=>1, 'created_at'=>Carbon::now()->format('Y:m:d H:i:s')],
        ]);
    }
}
