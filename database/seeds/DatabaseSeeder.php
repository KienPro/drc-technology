<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            UserSeeder::class,
            ProductCategorySeeder::class,
            ProductModelSeeder::class,
            RoleSeeder::class
        ]);
    }
}
